//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AragonPharmacyLibrary
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblRx
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblRx()
        {
            this.tblRefills = new HashSet<tblRefill>();
        }
    
        public string PrescriptionID { get; set; }
        public string DIN { get; set; }
        public string CustID { get; set; }
        public string DoctorID { get; set; }
        public decimal Quantity { get; set; }
        public string Unit { get; set; }
        public System.DateTime Date { get; set; }
        public System.DateTime ExpireDate { get; set; }
        public string Refills { get; set; }
        public bool AutoRefills { get; set; }
        public string RefillUsed { get; set; }
        public string Instructions { get; set; }
    
        public virtual tblCustomer tblCustomer { get; set; }
        public virtual tblDoctor tblDoctor { get; set; }
        public virtual tblDrug tblDrug { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblRefill> tblRefills { get; set; }
    }
}
