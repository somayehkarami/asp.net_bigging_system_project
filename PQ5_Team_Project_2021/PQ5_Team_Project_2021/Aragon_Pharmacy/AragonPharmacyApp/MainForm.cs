﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AragonPharmacyApp
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e) => lblTime.Text = DateTime.Now.ToString("hh:mm:ss");

        private void frmMain_Load(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void listOfEmployeeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var child = new DisplayEmployeeList();
            child.MdiParent = this;
            child.Show();
        }

        private void employeeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            var child = new frmEmployee();
            child.MdiParent = this;
            child.Show();

        }

        private void tsmiLogin_Click(object sender, EventArgs e)
        {
            var child = new frmLogin();
            child.MdiParent = this;
            child.Show();
        }

        // to be redo - need enable = false
        private void tsmiLogout_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void accountSettingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var child = new frmUser();
            child.MdiParent = this;
            child.Show();
        }

        private void btnPrescription_Click(object sender, EventArgs e)
        {
            var child = new frmPrescription();
            child.MdiParent = this;
            child.Show();
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            var child = new frmHelp();
            child.MdiParent = this;
            child.Show();
        }

        private void tsmiInsurance_Click(object sender, EventArgs e)
        {
            var child = new frmInsurance();
            child.MdiParent = this;
            child.Show();
        }
        private void tsmiHouse_Click(object sender, EventArgs e)
        {
            var child = new frmHouse();
            child.MdiParent = this;
            child.Show();
        }

        private void btnCustomer_Click(object sender, EventArgs e)
        {
            var child = new frmCustomer();
            child.MdiParent = this;
            child.Show();
        }

        private void btnDrug_Click(object sender, EventArgs e)
        {
            var child = new DisplayDrugList();
            child.MdiParent = this;
            child.Show();
        }

        private void btnTransations_Click(object sender, EventArgs e)
        {
            var child = new frmReport1();
            child.MdiParent = this;
            child.Show();
        }

        private void btnNote_Click(object sender, EventArgs e)
        {
            var child = new frmNote();
            child.MdiParent = this;
            child.Show();
        }
    }
}
