﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace AragonPharmacyApp
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void Login()
        {
            MessageBox.Show("welcome to Aragon Pharmacy System ", " ");
        }
        private void txtUsername_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < '0' || e.KeyChar > '9') & e.KeyChar != ControlChars.Back)
            {
                e.Handled = true;
            }
        }


        private void txtPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < '0' || e.KeyChar > '9') & e.KeyChar != ControlChars.Back)
            {
                e.Handled = true;
            }
        }

        private void txtUsername_Enter(object sender, EventArgs e)
        {
            txtUsername.SelectAll();
        }

        private void txtPassword_Enter(object sender, EventArgs e)
        {
            txtPassword.SelectAll();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Login();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}