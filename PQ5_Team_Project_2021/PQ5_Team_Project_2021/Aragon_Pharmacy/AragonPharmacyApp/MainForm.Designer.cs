﻿
namespace AragonPharmacyApp
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.MenuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tsmiFile = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiLogin = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiLogout = new System.Windows.Forms.ToolStripMenuItem();
            this.accountSettingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiExit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiManage = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEmployee = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.listOfEmployeeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCustomer = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newInsurancePlanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDrug = new System.Windows.Forms.ToolStripMenuItem();
            this.listOfDrugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listOfDrugToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiOrder = new System.Windows.Forms.ToolStripMenuItem();
            this.listOfOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trainingProgramToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trainingClassToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiResource = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiInsurance = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiHouse = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDoctor = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiClinic = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiReport = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSaleReport = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEmployeeReport = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnPrescription = new System.Windows.Forms.ToolStripButton();
            this.btnCustomer = new System.Windows.Forms.ToolStripButton();
            this.btnDrug = new System.Windows.Forms.ToolStripButton();
            this.btnTransations = new System.Windows.Forms.ToolStripButton();
            this.btnNote = new System.Windows.Forms.ToolStripButton();
            this.btnHelp = new System.Windows.Forms.ToolStripButton();
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ToolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblUser = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblTime = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.MenuStrip1.SuspendLayout();
            this.ToolStrip1.SuspendLayout();
            this.StatusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MenuStrip1
            // 
            this.MenuStrip1.BackColor = System.Drawing.Color.White;
            this.MenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiFile,
            this.tsmiManage,
            this.tsmiResource,
            this.tsmiReport,
            this.toolsToolStripMenuItem});
            this.MenuStrip1.Location = new System.Drawing.Point(0, 0);
            this.MenuStrip1.Name = "MenuStrip1";
            this.MenuStrip1.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.MenuStrip1.Size = new System.Drawing.Size(943, 24);
            this.MenuStrip1.TabIndex = 1;
            this.MenuStrip1.Text = "MenuStrip1";
            // 
            // tsmiFile
            // 
            this.tsmiFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiLogin,
            this.tsmiLogout,
            this.accountSettingToolStripMenuItem,
            this.tsmiExit});
            this.tsmiFile.Name = "tsmiFile";
            this.tsmiFile.Size = new System.Drawing.Size(37, 20);
            this.tsmiFile.Text = "&File";
            // 
            // tsmiLogin
            // 
            this.tsmiLogin.Name = "tsmiLogin";
            this.tsmiLogin.Size = new System.Drawing.Size(180, 22);
            this.tsmiLogin.Text = "Login";
            this.tsmiLogin.Click += new System.EventHandler(this.tsmiLogin_Click);
            // 
            // tsmiLogout
            // 
            this.tsmiLogout.Name = "tsmiLogout";
            this.tsmiLogout.Size = new System.Drawing.Size(180, 22);
            this.tsmiLogout.Text = "Logout";
            this.tsmiLogout.Click += new System.EventHandler(this.tsmiLogout_Click);
            // 
            // accountSettingToolStripMenuItem
            // 
            this.accountSettingToolStripMenuItem.Name = "accountSettingToolStripMenuItem";
            this.accountSettingToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.accountSettingToolStripMenuItem.Text = "Account Setting";
            this.accountSettingToolStripMenuItem.Click += new System.EventHandler(this.accountSettingToolStripMenuItem_Click);
            // 
            // tsmiExit
            // 
            this.tsmiExit.Name = "tsmiExit";
            this.tsmiExit.Size = new System.Drawing.Size(180, 22);
            this.tsmiExit.Text = "&Exit";
            this.tsmiExit.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // tsmiManage
            // 
            this.tsmiManage.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiEmployee,
            this.tsmiCustomer,
            this.tsmiDrug,
            this.tsmiOrder,
            this.trainingProgramToolStripMenuItem});
            this.tsmiManage.Name = "tsmiManage";
            this.tsmiManage.Size = new System.Drawing.Size(62, 20);
            this.tsmiManage.Text = "&Manage";
            // 
            // tsmiEmployee
            // 
            this.tsmiEmployee.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.employeeToolStripMenuItem1,
            this.listOfEmployeeToolStripMenuItem});
            this.tsmiEmployee.Name = "tsmiEmployee";
            this.tsmiEmployee.Size = new System.Drawing.Size(180, 22);
            this.tsmiEmployee.Text = "Employee";
            // 
            // employeeToolStripMenuItem1
            // 
            this.employeeToolStripMenuItem1.Name = "employeeToolStripMenuItem1";
            this.employeeToolStripMenuItem1.Size = new System.Drawing.Size(161, 22);
            this.employeeToolStripMenuItem1.Text = "New Employee";
            this.employeeToolStripMenuItem1.Click += new System.EventHandler(this.employeeToolStripMenuItem1_Click);
            // 
            // listOfEmployeeToolStripMenuItem
            // 
            this.listOfEmployeeToolStripMenuItem.Name = "listOfEmployeeToolStripMenuItem";
            this.listOfEmployeeToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.listOfEmployeeToolStripMenuItem.Text = "List of Employee";
            this.listOfEmployeeToolStripMenuItem.Click += new System.EventHandler(this.listOfEmployeeToolStripMenuItem_Click);
            // 
            // tsmiCustomer
            // 
            this.tsmiCustomer.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem,
            this.newInsurancePlanToolStripMenuItem});
            this.tsmiCustomer.Enabled = false;
            this.tsmiCustomer.Name = "tsmiCustomer";
            this.tsmiCustomer.Size = new System.Drawing.Size(180, 22);
            this.tsmiCustomer.Text = "Customer";
            // 
            // customerToolStripMenuItem
            // 
            this.customerToolStripMenuItem.Name = "customerToolStripMenuItem";
            this.customerToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.customerToolStripMenuItem.Text = "New Customer";
            // 
            // newInsurancePlanToolStripMenuItem
            // 
            this.newInsurancePlanToolStripMenuItem.Name = "newInsurancePlanToolStripMenuItem";
            this.newInsurancePlanToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.newInsurancePlanToolStripMenuItem.Text = "List of Customer";
            // 
            // tsmiDrug
            // 
            this.tsmiDrug.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listOfDrugToolStripMenuItem,
            this.listOfDrugToolStripMenuItem1});
            this.tsmiDrug.Enabled = false;
            this.tsmiDrug.Name = "tsmiDrug";
            this.tsmiDrug.Size = new System.Drawing.Size(180, 22);
            this.tsmiDrug.Text = "Product";
            // 
            // listOfDrugToolStripMenuItem
            // 
            this.listOfDrugToolStripMenuItem.Name = "listOfDrugToolStripMenuItem";
            this.listOfDrugToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.listOfDrugToolStripMenuItem.Text = "New Drug";
            // 
            // listOfDrugToolStripMenuItem1
            // 
            this.listOfDrugToolStripMenuItem1.Name = "listOfDrugToolStripMenuItem1";
            this.listOfDrugToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.listOfDrugToolStripMenuItem1.Text = "List of Drug";
            // 
            // tsmiOrder
            // 
            this.tsmiOrder.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listOfOrderToolStripMenuItem});
            this.tsmiOrder.Enabled = false;
            this.tsmiOrder.Name = "tsmiOrder";
            this.tsmiOrder.Size = new System.Drawing.Size(180, 22);
            this.tsmiOrder.Text = "Order";
            // 
            // listOfOrderToolStripMenuItem
            // 
            this.listOfOrderToolStripMenuItem.Name = "listOfOrderToolStripMenuItem";
            this.listOfOrderToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.listOfOrderToolStripMenuItem.Text = "Order";
            // 
            // trainingProgramToolStripMenuItem
            // 
            this.trainingProgramToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.trainingClassToolStripMenuItem});
            this.trainingProgramToolStripMenuItem.Name = "trainingProgramToolStripMenuItem";
            this.trainingProgramToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.trainingProgramToolStripMenuItem.Text = "Training Program";
            // 
            // trainingClassToolStripMenuItem
            // 
            this.trainingClassToolStripMenuItem.Name = "trainingClassToolStripMenuItem";
            this.trainingClassToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.trainingClassToolStripMenuItem.Text = "Training Class";
            // 
            // tsmiResource
            // 
            this.tsmiResource.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiInsurance,
            this.tsmiHouse,
            this.tsmiDoctor,
            this.tsmiClinic});
            this.tsmiResource.Name = "tsmiResource";
            this.tsmiResource.Size = new System.Drawing.Size(67, 20);
            this.tsmiResource.Text = "&Resource";
            // 
            // tsmiInsurance
            // 
            this.tsmiInsurance.Name = "tsmiInsurance";
            this.tsmiInsurance.Size = new System.Drawing.Size(180, 22);
            this.tsmiInsurance.Text = "Insurance ";
            this.tsmiInsurance.Click += new System.EventHandler(this.tsmiInsurance_Click);
            // 
            // tsmiHouse
            // 
            this.tsmiHouse.Name = "tsmiHouse";
            this.tsmiHouse.Size = new System.Drawing.Size(180, 22);
            this.tsmiHouse.Text = "House";
            this.tsmiHouse.Click += new System.EventHandler(this.tsmiHouse_Click);
            // 
            // tsmiDoctor
            // 
            this.tsmiDoctor.Enabled = false;
            this.tsmiDoctor.Name = "tsmiDoctor";
            this.tsmiDoctor.Size = new System.Drawing.Size(180, 22);
            this.tsmiDoctor.Text = "Doctor";
            // 
            // tsmiClinic
            // 
            this.tsmiClinic.Enabled = false;
            this.tsmiClinic.Name = "tsmiClinic";
            this.tsmiClinic.Size = new System.Drawing.Size(180, 22);
            this.tsmiClinic.Text = "Clinic";
            // 
            // tsmiReport
            // 
            this.tsmiReport.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiSaleReport,
            this.tsmiEmployeeReport});
            this.tsmiReport.Name = "tsmiReport";
            this.tsmiReport.Size = new System.Drawing.Size(54, 20);
            this.tsmiReport.Text = "Re&port";
            // 
            // tsmiSaleReport
            // 
            this.tsmiSaleReport.Enabled = false;
            this.tsmiSaleReport.Name = "tsmiSaleReport";
            this.tsmiSaleReport.Size = new System.Drawing.Size(180, 22);
            this.tsmiSaleReport.Text = "Sales Report";
            // 
            // tsmiEmployeeReport
            // 
            this.tsmiEmployeeReport.Enabled = false;
            this.tsmiEmployeeReport.Name = "tsmiEmployeeReport";
            this.tsmiEmployeeReport.Size = new System.Drawing.Size(180, 22);
            this.tsmiEmployeeReport.Text = "Employees Report";
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.backupToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.toolsToolStripMenuItem.Text = "&Tools";
            // 
            // backupToolStripMenuItem
            // 
            this.backupToolStripMenuItem.Enabled = false;
            this.backupToolStripMenuItem.Name = "backupToolStripMenuItem";
            this.backupToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.backupToolStripMenuItem.Text = "&Backup";
            // 
            // ToolStrip1
            // 
            this.ToolStrip1.AutoSize = false;
            this.ToolStrip1.BackColor = System.Drawing.Color.SlateGray;
            this.ToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.ToolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnPrescription,
            this.btnCustomer,
            this.btnDrug,
            this.btnTransations,
            this.btnNote,
            this.btnHelp});
            this.ToolStrip1.Location = new System.Drawing.Point(0, 24);
            this.ToolStrip1.Name = "ToolStrip1";
            this.ToolStrip1.Size = new System.Drawing.Size(943, 75);
            this.ToolStrip1.TabIndex = 5;
            this.ToolStrip1.Text = "ToolStrip1";
            // 
            // btnPrescription
            // 
            this.btnPrescription.AutoSize = false;
            this.btnPrescription.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrescription.ForeColor = System.Drawing.Color.White;
            this.btnPrescription.Image = ((System.Drawing.Image)(resources.GetObject("btnPrescription.Image")));
            this.btnPrescription.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnPrescription.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrescription.Name = "btnPrescription";
            this.btnPrescription.Size = new System.Drawing.Size(70, 72);
            this.btnPrescription.Text = "Prescription";
            this.btnPrescription.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnPrescription.ToolTipText = "Prescription";
            this.btnPrescription.Click += new System.EventHandler(this.btnPrescription_Click);
            // 
            // btnCustomer
            // 
            this.btnCustomer.AutoSize = false;
            this.btnCustomer.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCustomer.ForeColor = System.Drawing.Color.White;
            this.btnCustomer.Image = ((System.Drawing.Image)(resources.GetObject("btnCustomer.Image")));
            this.btnCustomer.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnCustomer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCustomer.Name = "btnCustomer";
            this.btnCustomer.Size = new System.Drawing.Size(80, 72);
            this.btnCustomer.Text = "Customer";
            this.btnCustomer.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnCustomer.ToolTipText = "Customer";
            this.btnCustomer.Click += new System.EventHandler(this.btnCustomer_Click);
            // 
            // btnDrug
            // 
            this.btnDrug.AutoSize = false;
            this.btnDrug.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDrug.ForeColor = System.Drawing.Color.White;
            this.btnDrug.Image = ((System.Drawing.Image)(resources.GetObject("btnDrug.Image")));
            this.btnDrug.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnDrug.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDrug.Name = "btnDrug";
            this.btnDrug.Size = new System.Drawing.Size(70, 72);
            this.btnDrug.Text = "Drug";
            this.btnDrug.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnDrug.Click += new System.EventHandler(this.btnDrug_Click);
            // 
            // btnTransations
            // 
            this.btnTransations.AutoSize = false;
            this.btnTransations.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTransations.ForeColor = System.Drawing.Color.White;
            this.btnTransations.Image = ((System.Drawing.Image)(resources.GetObject("btnTransations.Image")));
            this.btnTransations.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnTransations.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnTransations.Name = "btnTransations";
            this.btnTransations.Size = new System.Drawing.Size(80, 72);
            this.btnTransations.Text = "Report";
            this.btnTransations.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnTransations.Click += new System.EventHandler(this.btnTransations_Click);
            // 
            // btnNote
            // 
            this.btnNote.AutoSize = false;
            this.btnNote.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNote.ForeColor = System.Drawing.Color.White;
            this.btnNote.Image = ((System.Drawing.Image)(resources.GetObject("btnNote.Image")));
            this.btnNote.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnNote.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNote.Name = "btnNote";
            this.btnNote.Size = new System.Drawing.Size(70, 72);
            this.btnNote.Text = "Note";
            this.btnNote.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnNote.Click += new System.EventHandler(this.btnNote_Click);
            // 
            // btnHelp
            // 
            this.btnHelp.AutoSize = false;
            this.btnHelp.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHelp.ForeColor = System.Drawing.Color.White;
            this.btnHelp.Image = ((System.Drawing.Image)(resources.GetObject("btnHelp.Image")));
            this.btnHelp.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(70, 72);
            this.btnHelp.Text = "Help";
            this.btnHelp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripStatusLabel1,
            this.lblUser});
            this.StatusStrip1.Location = new System.Drawing.Point(0, 406);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
            this.StatusStrip1.Size = new System.Drawing.Size(943, 30);
            this.StatusStrip1.TabIndex = 7;
            this.StatusStrip1.Text = "StatusStrip1";
            // 
            // ToolStripStatusLabel1
            // 
            this.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1";
            this.ToolStripStatusLabel1.Size = new System.Drawing.Size(156, 25);
            this.ToolStripStatusLabel1.Text = "User From Sales Department";
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = false;
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(300, 25);
            this.lblUser.Tag = "3";
            this.lblUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTime
            // 
            this.lblTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTime.BackColor = System.Drawing.Color.SlateGray;
            this.lblTime.Font = new System.Drawing.Font("Verdana", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTime.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblTime.Location = new System.Drawing.Point(601, 33);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(306, 57);
            this.lblTime.TabIndex = 8;
            this.lblTime.Text = "00:00:00";
            this.lblTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(943, 436);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.StatusStrip1);
            this.Controls.Add(this.ToolStrip1);
            this.Controls.Add(this.MenuStrip1);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.MenuStrip1;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ARAGON PHARMACY CANADA 2021";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.MenuStrip1.ResumeLayout(false);
            this.MenuStrip1.PerformLayout();
            this.ToolStrip1.ResumeLayout(false);
            this.ToolStrip1.PerformLayout();
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.MenuStrip MenuStrip1;
        internal System.Windows.Forms.ToolStripMenuItem tsmiFile;
        internal System.Windows.Forms.ToolStripMenuItem tsmiLogin;
        internal System.Windows.Forms.ToolStripMenuItem tsmiManage;
        internal System.Windows.Forms.ToolStripMenuItem tsmiEmployee;
        internal System.Windows.Forms.ToolStripMenuItem tsmiCustomer;
        internal System.Windows.Forms.ToolStripMenuItem tsmiDrug;
        internal System.Windows.Forms.ToolStripMenuItem tsmiOrder;
        internal System.Windows.Forms.ToolStripMenuItem tsmiResource;
        internal System.Windows.Forms.ToolStripMenuItem tsmiInsurance;
        internal System.Windows.Forms.ToolStripMenuItem tsmiReport;
        internal System.Windows.Forms.ToolStripMenuItem tsmiSaleReport;
        internal System.Windows.Forms.ToolStripMenuItem tsmiLogout;
        private System.Windows.Forms.ToolStripMenuItem tsmiExit;
        private System.Windows.Forms.ToolStripMenuItem employeeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiHouse;
        private System.Windows.Forms.ToolStripMenuItem tsmiDoctor;
        private System.Windows.Forms.ToolStripMenuItem tsmiClinic;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backupToolStripMenuItem;
        internal System.Windows.Forms.ToolStrip ToolStrip1;
        internal System.Windows.Forms.ToolStripButton btnPrescription;
        internal System.Windows.Forms.ToolStripButton btnCustomer;
        internal System.Windows.Forms.ToolStripButton btnDrug;
        internal System.Windows.Forms.ToolStripButton btnHelp;
        internal System.Windows.Forms.ToolStripButton btnTransations;
        internal System.Windows.Forms.ToolStripButton btnNote;
        internal System.Windows.Forms.StatusStrip StatusStrip1;
        internal System.Windows.Forms.ToolStripStatusLabel ToolStripStatusLabel1;
        internal System.Windows.Forms.ToolStripStatusLabel lblUser;
        private System.Windows.Forms.ToolStripMenuItem tsmiEmployeeReport;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripMenuItem listOfDrugToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listOfOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listOfEmployeeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newInsurancePlanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountSettingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listOfDrugToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem trainingProgramToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem trainingClassToolStripMenuItem;
    }
}