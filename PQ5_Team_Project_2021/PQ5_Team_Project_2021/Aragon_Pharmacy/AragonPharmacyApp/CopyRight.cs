﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AragonPharmacyApp
{
    public partial class CopyRight : Form
    {
        public CopyRight()
        {
            InitializeComponent();
            this.timerAbout.Enabled = true;
            this.timerAbout.Interval = 3000;
        }

        

        private void timerAbout_Tick(object sender, EventArgs e)
        {
            timerAbout.Stop();
            new frmMain().Show();
            this.Hide();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            linkLabel1.LinkVisited = true;
            // open the note pad for now 
            System.Diagnostics.Process.Start("notepad");
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            linkLabel2.LinkVisited = true;
            // open the google web for now 
            System.Diagnostics.Process.Start("Http://www.google.ca");
        }
    }
}
