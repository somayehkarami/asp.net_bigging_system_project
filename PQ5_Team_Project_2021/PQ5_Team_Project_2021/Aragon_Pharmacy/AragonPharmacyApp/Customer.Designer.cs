﻿
namespace AragonPharmacyApp
{
    partial class frmCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label allergiesLabel;
            System.Windows.Forms.Label balanceLabel;
            System.Windows.Forms.Label childProofCapLabel;
            System.Windows.Forms.Label custFisrtLabel;
            System.Windows.Forms.Label custIDLabel;
            System.Windows.Forms.Label custLastLabel;
            System.Windows.Forms.Label dOBLabel;
            System.Windows.Forms.Label genderLabel;
            System.Windows.Forms.Label headHHLabel;
            System.Windows.Forms.Label houseIDLabel;
            System.Windows.Forms.Label phoneLabel;
            System.Windows.Forms.Label planIDLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomer));
            this.tblCustomerBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.allergiesTextBox = new System.Windows.Forms.TextBox();
            this.balanceTextBox = new System.Windows.Forms.TextBox();
            this.childProofCapCheckBox = new System.Windows.Forms.CheckBox();
            this.custFisrtTextBox = new System.Windows.Forms.TextBox();
            this.custIDTextBox = new System.Windows.Forms.TextBox();
            this.custLastTextBox = new System.Windows.Forms.TextBox();
            this.dOBDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.genderTextBox = new System.Windows.Forms.TextBox();
            this.headHHCheckBox = new System.Windows.Forms.CheckBox();
            this.houseIDTextBox = new System.Windows.Forms.TextBox();
            this.phoneTextBox = new System.Windows.Forms.TextBox();
            this.planIDTextBox = new System.Windows.Forms.TextBox();
            this.tblCustomerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.tblCustomerBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnFindAll = new System.Windows.Forms.Button();
            this.btnFind = new System.Windows.Forms.Button();
            this.txtSearchLastName = new System.Windows.Forms.TextBox();
            this.lblSearchLastName = new System.Windows.Forms.Label();
            allergiesLabel = new System.Windows.Forms.Label();
            balanceLabel = new System.Windows.Forms.Label();
            childProofCapLabel = new System.Windows.Forms.Label();
            custFisrtLabel = new System.Windows.Forms.Label();
            custIDLabel = new System.Windows.Forms.Label();
            custLastLabel = new System.Windows.Forms.Label();
            dOBLabel = new System.Windows.Forms.Label();
            genderLabel = new System.Windows.Forms.Label();
            headHHLabel = new System.Windows.Forms.Label();
            houseIDLabel = new System.Windows.Forms.Label();
            phoneLabel = new System.Windows.Forms.Label();
            planIDLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.tblCustomerBindingNavigator)).BeginInit();
            this.tblCustomerBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblCustomerBindingSource)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tblCustomerBindingNavigator
            // 
            this.tblCustomerBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.tblCustomerBindingNavigator.BindingSource = this.tblCustomerBindingSource;
            this.tblCustomerBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.tblCustomerBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.tblCustomerBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.tblCustomerBindingNavigatorSaveItem});
            this.tblCustomerBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.tblCustomerBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.tblCustomerBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.tblCustomerBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.tblCustomerBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.tblCustomerBindingNavigator.Name = "tblCustomerBindingNavigator";
            this.tblCustomerBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.tblCustomerBindingNavigator.Size = new System.Drawing.Size(650, 25);
            this.tblCustomerBindingNavigator.TabIndex = 0;
            this.tblCustomerBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(58, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // allergiesLabel
            // 
            allergiesLabel.AutoSize = true;
            allergiesLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            allergiesLabel.Location = new System.Drawing.Point(41, 171);
            allergiesLabel.Name = "allergiesLabel";
            allergiesLabel.Size = new System.Drawing.Size(61, 13);
            allergiesLabel.TabIndex = 1;
            allergiesLabel.Text = "Allergies:";
            // 
            // allergiesTextBox
            // 
            this.allergiesTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.allergiesTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblCustomerBindingSource, "Allergies", true));
            this.allergiesTextBox.Location = new System.Drawing.Point(119, 169);
            this.allergiesTextBox.Multiline = true;
            this.allergiesTextBox.Name = "allergiesTextBox";
            this.allergiesTextBox.Size = new System.Drawing.Size(458, 89);
            this.allergiesTextBox.TabIndex = 2;
            this.allergiesTextBox.Text = "List all Allergies";
            // 
            // balanceLabel
            // 
            balanceLabel.AutoSize = true;
            balanceLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            balanceLabel.Location = new System.Drawing.Point(345, 112);
            balanceLabel.Name = "balanceLabel";
            balanceLabel.Size = new System.Drawing.Size(57, 13);
            balanceLabel.TabIndex = 3;
            balanceLabel.Text = "Balance:";
            // 
            // balanceTextBox
            // 
            this.balanceTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblCustomerBindingSource, "Balance", true));
            this.balanceTextBox.Location = new System.Drawing.Point(427, 108);
            this.balanceTextBox.Name = "balanceTextBox";
            this.balanceTextBox.Size = new System.Drawing.Size(150, 21);
            this.balanceTextBox.TabIndex = 4;
            // 
            // childProofCapLabel
            // 
            childProofCapLabel.AutoSize = true;
            childProofCapLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            childProofCapLabel.Location = new System.Drawing.Point(459, 83);
            childProofCapLabel.Name = "childProofCapLabel";
            childProofCapLabel.Size = new System.Drawing.Size(102, 13);
            childProofCapLabel.TabIndex = 5;
            childProofCapLabel.Text = "Child Proof Cap:";
            // 
            // childProofCapCheckBox
            // 
            this.childProofCapCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.tblCustomerBindingSource, "ChildProofCap", true));
            this.childProofCapCheckBox.Location = new System.Drawing.Point(563, 77);
            this.childProofCapCheckBox.Name = "childProofCapCheckBox";
            this.childProofCapCheckBox.Size = new System.Drawing.Size(28, 24);
            this.childProofCapCheckBox.TabIndex = 6;
            this.childProofCapCheckBox.UseVisualStyleBackColor = true;
            // 
            // custFisrtLabel
            // 
            custFisrtLabel.AutoSize = true;
            custFisrtLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            custFisrtLabel.Location = new System.Drawing.Point(30, 57);
            custFisrtLabel.Name = "custFisrtLabel";
            custFisrtLabel.Size = new System.Drawing.Size(73, 13);
            custFisrtLabel.TabIndex = 7;
            custFisrtLabel.Text = "First Name:";
            // 
            // custFisrtTextBox
            // 
            this.custFisrtTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblCustomerBindingSource, "CustFisrt", true));
            this.custFisrtTextBox.Location = new System.Drawing.Point(119, 53);
            this.custFisrtTextBox.Name = "custFisrtTextBox";
            this.custFisrtTextBox.Size = new System.Drawing.Size(150, 21);
            this.custFisrtTextBox.TabIndex = 8;
            // 
            // custIDLabel
            // 
            custIDLabel.AutoSize = true;
            custIDLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            custIDLabel.Location = new System.Drawing.Point(17, 32);
            custIDLabel.Name = "custIDLabel";
            custIDLabel.Size = new System.Drawing.Size(86, 13);
            custIDLabel.TabIndex = 9;
            custIDLabel.Text = "Customer ID:";
            // 
            // custIDTextBox
            // 
            this.custIDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblCustomerBindingSource, "CustID", true));
            this.custIDTextBox.Location = new System.Drawing.Point(119, 24);
            this.custIDTextBox.Name = "custIDTextBox";
            this.custIDTextBox.Size = new System.Drawing.Size(150, 21);
            this.custIDTextBox.TabIndex = 10;
            // 
            // custLastLabel
            // 
            custLastLabel.AutoSize = true;
            custLastLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            custLastLabel.Location = new System.Drawing.Point(31, 87);
            custLastLabel.Name = "custLastLabel";
            custLastLabel.Size = new System.Drawing.Size(72, 13);
            custLastLabel.TabIndex = 11;
            custLastLabel.Text = "Last Name:";
            // 
            // custLastTextBox
            // 
            this.custLastTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblCustomerBindingSource, "CustLast", true));
            this.custLastTextBox.Location = new System.Drawing.Point(119, 83);
            this.custLastTextBox.Name = "custLastTextBox";
            this.custLastTextBox.Size = new System.Drawing.Size(150, 21);
            this.custLastTextBox.TabIndex = 12;
            // 
            // dOBLabel
            // 
            dOBLabel.AutoSize = true;
            dOBLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dOBLabel.Location = new System.Drawing.Point(65, 116);
            dOBLabel.Name = "dOBLabel";
            dOBLabel.Size = new System.Drawing.Size(38, 13);
            dOBLabel.TabIndex = 13;
            dOBLabel.Text = "DOB:";
            // 
            // dOBDateTimePicker
            // 
            this.dOBDateTimePicker.CustomFormat = " MM / dd / yy";
            this.dOBDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.tblCustomerBindingSource, "DOB", true));
            this.dOBDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dOBDateTimePicker.Location = new System.Drawing.Point(119, 112);
            this.dOBDateTimePicker.Name = "dOBDateTimePicker";
            this.dOBDateTimePicker.Size = new System.Drawing.Size(150, 21);
            this.dOBDateTimePicker.TabIndex = 14;
            // 
            // genderLabel
            // 
            genderLabel.AutoSize = true;
            genderLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            genderLabel.Location = new System.Drawing.Point(49, 146);
            genderLabel.Name = "genderLabel";
            genderLabel.Size = new System.Drawing.Size(54, 13);
            genderLabel.TabIndex = 15;
            genderLabel.Text = "Gender:";
            // 
            // genderTextBox
            // 
            this.genderTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblCustomerBindingSource, "Gender", true));
            this.genderTextBox.Location = new System.Drawing.Point(119, 142);
            this.genderTextBox.Name = "genderTextBox";
            this.genderTextBox.Size = new System.Drawing.Size(150, 21);
            this.genderTextBox.TabIndex = 16;
            // 
            // headHHLabel
            // 
            headHHLabel.AutoSize = true;
            headHHLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            headHHLabel.Location = new System.Drawing.Point(341, 83);
            headHHLabel.Name = "headHHLabel";
            headHHLabel.Size = new System.Drawing.Size(61, 13);
            headHHLabel.TabIndex = 17;
            headHHLabel.Text = "Head HH:";
            // 
            // headHHCheckBox
            // 
            this.headHHCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.tblCustomerBindingSource, "HeadHH", true));
            this.headHHCheckBox.Location = new System.Drawing.Point(427, 77);
            this.headHHCheckBox.Name = "headHHCheckBox";
            this.headHHCheckBox.Size = new System.Drawing.Size(23, 24);
            this.headHHCheckBox.TabIndex = 18;
            this.headHHCheckBox.UseVisualStyleBackColor = true;
            // 
            // houseIDLabel
            // 
            houseIDLabel.AutoSize = true;
            houseIDLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            houseIDLabel.Location = new System.Drawing.Point(337, 24);
            houseIDLabel.Name = "houseIDLabel";
            houseIDLabel.Size = new System.Drawing.Size(65, 13);
            houseIDLabel.TabIndex = 19;
            houseIDLabel.Text = "House ID:";
            // 
            // houseIDTextBox
            // 
            this.houseIDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblCustomerBindingSource, "HouseID", true));
            this.houseIDTextBox.Location = new System.Drawing.Point(427, 20);
            this.houseIDTextBox.Name = "houseIDTextBox";
            this.houseIDTextBox.Size = new System.Drawing.Size(150, 21);
            this.houseIDTextBox.TabIndex = 20;
            // 
            // phoneLabel
            // 
            phoneLabel.AutoSize = true;
            phoneLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            phoneLabel.Location = new System.Drawing.Point(355, 142);
            phoneLabel.Name = "phoneLabel";
            phoneLabel.Size = new System.Drawing.Size(47, 13);
            phoneLabel.TabIndex = 21;
            phoneLabel.Text = "Phone:";
            // 
            // phoneTextBox
            // 
            this.phoneTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblCustomerBindingSource, "Phone", true));
            this.phoneTextBox.Location = new System.Drawing.Point(427, 142);
            this.phoneTextBox.Name = "phoneTextBox";
            this.phoneTextBox.Size = new System.Drawing.Size(150, 21);
            this.phoneTextBox.TabIndex = 22;
            // 
            // planIDLabel
            // 
            planIDLabel.AutoSize = true;
            planIDLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            planIDLabel.Location = new System.Drawing.Point(348, 53);
            planIDLabel.Name = "planIDLabel";
            planIDLabel.Size = new System.Drawing.Size(54, 13);
            planIDLabel.TabIndex = 23;
            planIDLabel.Text = "Plan ID:";
            // 
            // planIDTextBox
            // 
            this.planIDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblCustomerBindingSource, "PlanID", true));
            this.planIDTextBox.Location = new System.Drawing.Point(427, 49);
            this.planIDTextBox.Name = "planIDTextBox";
            this.planIDTextBox.Size = new System.Drawing.Size(150, 21);
            this.planIDTextBox.TabIndex = 24;
            // 
            // tblCustomerBindingSource
            // 
            this.tblCustomerBindingSource.DataSource = typeof(AragonPharmacyLibrary.tblCustomer);
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // tblCustomerBindingNavigatorSaveItem
            // 
            this.tblCustomerBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tblCustomerBindingNavigatorSaveItem.Enabled = false;
            this.tblCustomerBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("tblCustomerBindingNavigatorSaveItem.Image")));
            this.tblCustomerBindingNavigatorSaveItem.Name = "tblCustomerBindingNavigatorSaveItem";
            this.tblCustomerBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.tblCustomerBindingNavigatorSaveItem.Text = "Save Data";
            this.tblCustomerBindingNavigatorSaveItem.Click += new System.EventHandler(this.tblRxBindingNavigatorSaveItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.houseIDTextBox);
            this.groupBox1.Controls.Add(allergiesLabel);
            this.groupBox1.Controls.Add(this.planIDTextBox);
            this.groupBox1.Controls.Add(this.allergiesTextBox);
            this.groupBox1.Controls.Add(planIDLabel);
            this.groupBox1.Controls.Add(balanceLabel);
            this.groupBox1.Controls.Add(this.phoneTextBox);
            this.groupBox1.Controls.Add(this.balanceTextBox);
            this.groupBox1.Controls.Add(phoneLabel);
            this.groupBox1.Controls.Add(childProofCapLabel);
            this.groupBox1.Controls.Add(houseIDLabel);
            this.groupBox1.Controls.Add(this.childProofCapCheckBox);
            this.groupBox1.Controls.Add(this.headHHCheckBox);
            this.groupBox1.Controls.Add(custFisrtLabel);
            this.groupBox1.Controls.Add(headHHLabel);
            this.groupBox1.Controls.Add(this.custFisrtTextBox);
            this.groupBox1.Controls.Add(this.genderTextBox);
            this.groupBox1.Controls.Add(custIDLabel);
            this.groupBox1.Controls.Add(genderLabel);
            this.groupBox1.Controls.Add(this.custIDTextBox);
            this.groupBox1.Controls.Add(this.dOBDateTimePicker);
            this.groupBox1.Controls.Add(custLastLabel);
            this.groupBox1.Controls.Add(dOBLabel);
            this.groupBox1.Controls.Add(this.custLastTextBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 38);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(626, 286);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "CUSTOMER";
            // 
            // btnFindAll
            // 
            this.btnFindAll.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnFindAll.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFindAll.Location = new System.Drawing.Point(514, 340);
            this.btnFindAll.Name = "btnFindAll";
            this.btnFindAll.Size = new System.Drawing.Size(75, 23);
            this.btnFindAll.TabIndex = 63;
            this.btnFindAll.Text = "Search &All";
            this.btnFindAll.UseVisualStyleBackColor = true;
            this.btnFindAll.Click += new System.EventHandler(this.btnFindAll_Click);
            // 
            // btnFind
            // 
            this.btnFind.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnFind.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFind.Location = new System.Drawing.Point(413, 340);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(75, 23);
            this.btnFind.TabIndex = 61;
            this.btnFind.Text = "&Find ";
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // txtSearchLastName
            // 
            this.txtSearchLastName.Location = new System.Drawing.Point(131, 340);
            this.txtSearchLastName.Name = "txtSearchLastName";
            this.txtSearchLastName.Size = new System.Drawing.Size(150, 21);
            this.txtSearchLastName.TabIndex = 62;
            // 
            // lblSearchLastName
            // 
            this.lblSearchLastName.AutoSize = true;
            this.lblSearchLastName.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchLastName.Location = new System.Drawing.Point(38, 343);
            this.lblSearchLastName.Name = "lblSearchLastName";
            this.lblSearchLastName.Size = new System.Drawing.Size(76, 13);
            this.lblSearchLastName.TabIndex = 60;
            this.lblSearchLastName.Text = "Last Name :";
            // 
            // frmCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.Info;
            this.ClientSize = new System.Drawing.Size(650, 376);
            this.Controls.Add(this.btnFindAll);
            this.Controls.Add(this.btnFind);
            this.Controls.Add(this.txtSearchLastName);
            this.Controls.Add(this.lblSearchLastName);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tblCustomerBindingNavigator);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmCustomer";
            this.Text = "Customer";
            this.Load += new System.EventHandler(this.frmCustomer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tblCustomerBindingNavigator)).EndInit();
            this.tblCustomerBindingNavigator.ResumeLayout(false);
            this.tblCustomerBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblCustomerBindingSource)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource tblCustomerBindingSource;
        private System.Windows.Forms.BindingNavigator tblCustomerBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton tblCustomerBindingNavigatorSaveItem;
        private System.Windows.Forms.TextBox allergiesTextBox;
        private System.Windows.Forms.TextBox balanceTextBox;
        private System.Windows.Forms.CheckBox childProofCapCheckBox;
        private System.Windows.Forms.TextBox custFisrtTextBox;
        private System.Windows.Forms.TextBox custIDTextBox;
        private System.Windows.Forms.TextBox custLastTextBox;
        private System.Windows.Forms.DateTimePicker dOBDateTimePicker;
        private System.Windows.Forms.TextBox genderTextBox;
        private System.Windows.Forms.CheckBox headHHCheckBox;
        private System.Windows.Forms.TextBox houseIDTextBox;
        private System.Windows.Forms.TextBox phoneTextBox;
        private System.Windows.Forms.TextBox planIDTextBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnFindAll;
        private System.Windows.Forms.Button btnFind;
        private System.Windows.Forms.TextBox txtSearchLastName;
        private System.Windows.Forms.Label lblSearchLastName;
    }
}