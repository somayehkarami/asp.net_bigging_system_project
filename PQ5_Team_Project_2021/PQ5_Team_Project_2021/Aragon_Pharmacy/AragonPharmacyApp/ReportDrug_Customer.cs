﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AragonPharmacyApp
{
    public partial class frmReport1 : Form
    {
        public frmReport1()
        {
            InitializeComponent();
        }
        public SqlConnection Conn;

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDrugCost_Click(object sender, EventArgs e)
        {
            var dbcontext = new AragonPharmacyLibrary.AragonPharmacyDBEntities();
            var drugAndRx =
                from drug in dbcontext.tblDrugs
                from rx in drug.tblRxes

                select new {
                    rx.DIN,
                    drug.Name,
                    drug.Price,
                    drug.Cost
                };

            txtOutputResult.AppendText("Drugs and Cost:\r\n");
            txtOutputResult.AppendText("===============\r\n");
            txtOutputResult.AppendText("\r\n\tFIN\t\tName\t\t\tPrice\t\tCost");

            foreach (var element in drugAndRx)
            {
                txtOutputResult.AppendText($"\r\n\t{element.DIN,-25} { element.Name,-40} {element.Price,-10} {element.Cost,20}");
            }
        }

        private void btnRxCustomers_Click(object sender, EventArgs e)
        {
            var dbcontext = new AragonPharmacyLibrary.AragonPharmacyDBEntities();
            var rxAndCustomer =
                from cust in dbcontext.tblCustomers
                from rx in cust.tblRxes
                orderby cust.CustID
                select new {
                    cust.CustID,
                    cust.CustFisrt,
                    cust.CustLast,
                    rx.AutoRefills,
                    rx.Quantity,
                    rx.RefillUsed

                };

            txtOutputResult.AppendText("\r\n\r\nRx and Customers:\r\n");
            txtOutputResult.AppendText("===================\r\n");
            txtOutputResult.AppendText("\r\n\tCustId\t\tFirsrtName\tLastName\tAutoRefills\tQuantity\t\tRefilUsed");

            foreach (var element in rxAndCustomer)
            {
                txtOutputResult.AppendText($"\r\n\t{element.CustID,-30} {element.CustFisrt,-30} {element.CustLast,-30} " +
                    $"{element.AutoRefills,-30} {element.Quantity,-30} {element.RefillUsed,-30}");
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtOutputResult.Text = "";
        }
    }
}
