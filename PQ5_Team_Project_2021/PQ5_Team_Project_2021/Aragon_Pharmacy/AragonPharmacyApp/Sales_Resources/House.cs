﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AragonPharmacyApp
{
    public partial class frmHouse : Form
    {
        public frmHouse()
        {
            InitializeComponent();
        }

        // Entity Framework dbContext
        private AragonPharmacyLibrary.AragonPharmacyDBEntities dbcontext = null;

        // fill our tblEmployeeBindingSource with all rows, ordered by last
        // name first, then by first name
        private void RefreshHouse()
        {
            // Dispose old dbContext, if any
            if (dbcontext != null)
            {
                dbcontext.Dispose();
            }

            // create a new dbContext so we can reorder records based on edits
            dbcontext = new AragonPharmacyLibrary.AragonPharmacyDBEntities();

            // use LINQ to order 
            // by last name, then first name
            dbcontext.tblHouseholds
                .OrderBy(house => house.HouseID)
                .Load();

            // specify DataSource for tblEmployeeBindingSource
            tblHouseholdBindingSource.DataSource = dbcontext.tblHouseholds.Local;

            tblHouseholdBindingSource.MoveFirst(); // go to first record

            
        }
        private void frmHouse_Load(object sender, EventArgs e)
        {
            // when the form loads, fill it with data from the database
            RefreshHouse(); // fill binding with data from database

            // enable the Save button
            tblHouseholdBindingNavigatorSaveItem.Enabled = true;
        }

       
        private void tblHouseholdBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            // click event handler for the Save buttonn in the 
            // BindingNavigator saves the changes made to the data 
            Validate(); // validate input fields
            tblHouseholdBindingSource.EndEdit(); // complete current edit, if any

            // try to save changes
            try
            {
                dbcontext.SaveChanges();  // write changes to the database
            }
            catch (DbEntityValidationException)
            {
                MessageBox.Show("Columns cannot be empty", "Entity Validation Exception");
            }

            RefreshHouse();  // change back to initial unfiltered data
        }


        private void toolStripButton1_Click(object sender, EventArgs e)
        {

            bindingNavigatorAddNewItem.Enabled = true;
            bindingNavigatorDeleteItem.Enabled = true;

            RefreshHouse(); // change back to initial unfiltered data

            tblHouseholdBindingSource.DataSource =
                      dbcontext.tblHouseholds.Local
                      .OrderBy(house => house.PostalCode)
                      .ThenBy(house => house.HouseID);
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            RefreshHouse();

            tblHouseholdBindingSource.DataSource =
                       dbcontext.tblHouseholds.Local
                       .OrderBy(house => house.City)
                       .ThenBy(house => house.PostalCode);


            bindingNavigatorAddNewItem.Enabled = false;
            bindingNavigatorDeleteItem.Enabled = false;
        }
    }
}
