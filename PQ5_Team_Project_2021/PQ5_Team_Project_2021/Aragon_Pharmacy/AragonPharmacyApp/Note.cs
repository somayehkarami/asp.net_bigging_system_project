﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AragonPharmacyApp
{
    public partial class frmNote : Form
    {
        public frmNote()
        {
            InitializeComponent();
        }

        private void frmNote_Load(object sender, EventArgs e)
        {
            lblTimenow.Text = DateTime.Now.ToString("g");
           // lblTimenow.Text = DateTime.Now.ToString("hh:mm:ss");

        }
        private void txtFirstName_TextChanged(object sender, EventArgs e)
        {
            if (txtFirstName.Text != null && txtReceiver.Text != null)
            {
                lblWritenBy.Text = txtFirstName.Text;
            }
        }
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            string path = @"C:\\Users\\user\\Desktop\\vbnet\\PQ5_Team_Project_2021\\Note\\" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
            if (!File.Exists(path))
            {
                StreamWriter file = new StreamWriter(path);
                file.WriteLine("From: " + lblWritenBy.Text + "\nTo: " + txtReceiver.Text + "\n " + lblTimenow.Text + "\n " + txtNote.Text);
                //file.WriteLine(txtNote.Text);
                file.Close();
                MessageBox.Show("File is sent.","Form Closing", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("File already exists", "Duplicate Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            try
            {
                txtNote.Text = string.Empty;
                string path = @"C:\\Users\\user\\Desktop\\vbnet\\PQ5_Team_Project_2021\\Note\\" + txtFileRead.Text + ".txt";
                StreamReader stream = new StreamReader(path);
                string filedata = stream.ReadToEnd();
                txtNote.Text = filedata.ToString();
                stream.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtReceiver.Text = string.Empty;
            txtNote.Text = string.Empty;
            txtFirstName.Text = string.Empty;
            txtFileRead.Text = string.Empty;
        }

        private void btnRead_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Read File", btnRead);
            btnRead.BackColor = Color.LightBlue;
        }

        private void btnClear_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Clear Fields", btnClear);
            btnClear.BackColor = Color.LightBlue;
        }

        private void btnSubmit_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Send Note", btnSubmit);
            btnSubmit.BackColor = Color.LightBlue;
        }

        private void btnExit_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Exit", btnExit);
            btnExit.BackColor = Color.LightBlue;
        }

        private void btnExit_MouseLeave(object sender, EventArgs e)
        {
            btnExit.BackColor = Color.Transparent;
        }

        private void btnRead_MouseLeave(object sender, EventArgs e)
        {
            btnRead.BackColor = Color.Transparent;
        }

        private void btnClear_MouseLeave(object sender, EventArgs e)
        {
            btnClear.BackColor = Color.Transparent;
        }

        private void btnSubmit_MouseLeave(object sender, EventArgs e)
        {
            btnSubmit.BackColor = Color.Transparent;
        }
    }
}

