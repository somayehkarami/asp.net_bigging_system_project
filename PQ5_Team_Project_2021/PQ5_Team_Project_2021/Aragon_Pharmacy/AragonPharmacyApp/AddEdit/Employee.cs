﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AragonPharmacyApp
{
    public partial class frmEmployee : Form
    {
        public frmEmployee()
        {
            InitializeComponent();
        }

        // Entity Framework dbContext
        private AragonPharmacyLibrary.AragonPharmacyDBEntities dbcontext = null;

        // fill our tblEmployeeBindingSource with all rows, ordered by last
        // name first, then by first name
        private void RefreshEmployees()
        {
            // Dispose old dbContext, if any
            if (dbcontext != null)
            {
                dbcontext.Dispose();
            }

            // create a new dbContext so we can reorder records based on edits
            dbcontext = new AragonPharmacyLibrary.AragonPharmacyDBEntities();

            // use LINQ to order 
            // by last name, then first name
            dbcontext.tblEmployees
                .OrderBy(employee => employee.EmpID)
                .Load();

            // specify DataSource for tblEmployeeBindingSource
            tblEmployeeBindingSource.DataSource = dbcontext.tblEmployees.Local;

            tblEmployeeBindingSource.MoveFirst(); // go to first record

            txtSearchLastName.Clear(); // clear the find textbox
        }

       

        private void frmEmployee_Load(object sender, EventArgs e)
        {
            // when the form loads, fill it with data from the database
            RefreshEmployees(); // fill binding with data from database

            // enable the Save button
            tblEmployeeBindingNavigatorSaveItem.Enabled = true;
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            var lastNameQuery =
               from  employee in dbcontext.tblEmployees
               where employee.EmpLast.StartsWith(txtSearchLastName.Text)
               orderby employee.EmpLast, employee.EmpFirst
               select employee;

            // display matching contacts
            tblEmployeeBindingSource.DataSource = lastNameQuery.ToList();
            tblEmployeeBindingSource.MoveFirst();   // go to first record

            // don't allow add / delete when Employees are filtered 
            bindingNavigatorAddNewItem.Enabled = false;
            bindingNavigatorDeleteItem.Enabled = false;
        }


        private void tblEmployeeBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            // click event handler for the Save buttonn in the 
            // BindingNavigator saves the changes made to the data 
            Validate(); // validate input fields
            tblEmployeeBindingSource.EndEdit(); // complete current edit, if any

            // try to save changes
            try
            {
                dbcontext.SaveChanges();  // write changes to the database
            }
            catch (DbEntityValidationException)
            {
                MessageBox.Show("Columns cannot be empty", "Entity Validation Exception");
            }

            RefreshEmployees(); // change back to initial unfiltered data
        }

        private void btnFindAll_Click(object sender, EventArgs e)
        {
            // reload addressBindingSource with all rows
            // allow add / delete when contacts are not filtered 
            bindingNavigatorAddNewItem.Enabled = true;
            bindingNavigatorDeleteItem.Enabled = true;

            RefreshEmployees(); // change back to initial unfiltered data
        }
    }
}
