﻿
namespace AragonPharmacyApp
{
    partial class frmEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label addressLabel;
            System.Windows.Forms.Label cellLabel;
            System.Windows.Forms.Label cityLabel;
            System.Windows.Forms.Label dOBLabel;
            System.Windows.Forms.Label empFirstLabel;
            System.Windows.Forms.Label empIDLabel;
            System.Windows.Forms.Label empLastLabel;
            System.Windows.Forms.Label empMlLabel;
            System.Windows.Forms.Label hourlyRateLabel;
            System.Windows.Forms.Label phoneLabel;
            System.Windows.Forms.Label postalCodeLabel;
            System.Windows.Forms.Label provinceLabel;
            System.Windows.Forms.Label titleLabel;
            System.Windows.Forms.Label commentsLabel;
            System.Windows.Forms.Label endDateLabel;
            System.Windows.Forms.Label memoLabel;
            System.Windows.Forms.Label reviewLabel;
            System.Windows.Forms.Label salaryLabel;
            System.Windows.Forms.Label sINsLabel;
            System.Windows.Forms.Label startDateLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEmployee));
            this.addressTextBox = new System.Windows.Forms.TextBox();
            this.cellTextBox = new System.Windows.Forms.TextBox();
            this.cityTextBox = new System.Windows.Forms.TextBox();
            this.dOBDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.empFirstTextBox = new System.Windows.Forms.TextBox();
            this.empIDTextBox = new System.Windows.Forms.TextBox();
            this.empLastTextBox = new System.Windows.Forms.TextBox();
            this.empMlTextBox = new System.Windows.Forms.TextBox();
            this.phoneTextBox = new System.Windows.Forms.TextBox();
            this.postalCodeTextBox = new System.Windows.Forms.TextBox();
            this.provinceComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.titleComboBox = new System.Windows.Forms.ComboBox();
            this.commentsTextBox = new System.Windows.Forms.TextBox();
            this.endDateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.hourlyRateTextBox = new System.Windows.Forms.TextBox();
            this.memoTextBox = new System.Windows.Forms.TextBox();
            this.reviewDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.salaryTextBox = new System.Windows.Forms.TextBox();
            this.sINsTextBox = new System.Windows.Forms.TextBox();
            this.startDateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.tblEmployeeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.Panel1 = new System.Windows.Forms.Panel();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblSearchLastName = new System.Windows.Forms.Label();
            this.btnFind = new System.Windows.Forms.Button();
            this.btnFindAll = new System.Windows.Forms.Button();
            this.tblEmployeeBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tblEmployeeBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtSearchLastName = new System.Windows.Forms.TextBox();
            addressLabel = new System.Windows.Forms.Label();
            cellLabel = new System.Windows.Forms.Label();
            cityLabel = new System.Windows.Forms.Label();
            dOBLabel = new System.Windows.Forms.Label();
            empFirstLabel = new System.Windows.Forms.Label();
            empIDLabel = new System.Windows.Forms.Label();
            empLastLabel = new System.Windows.Forms.Label();
            empMlLabel = new System.Windows.Forms.Label();
            hourlyRateLabel = new System.Windows.Forms.Label();
            phoneLabel = new System.Windows.Forms.Label();
            postalCodeLabel = new System.Windows.Forms.Label();
            provinceLabel = new System.Windows.Forms.Label();
            titleLabel = new System.Windows.Forms.Label();
            commentsLabel = new System.Windows.Forms.Label();
            endDateLabel = new System.Windows.Forms.Label();
            memoLabel = new System.Windows.Forms.Label();
            reviewLabel = new System.Windows.Forms.Label();
            salaryLabel = new System.Windows.Forms.Label();
            sINsLabel = new System.Windows.Forms.Label();
            startDateLabel = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblEmployeeBindingSource)).BeginInit();
            this.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblEmployeeBindingNavigator)).BeginInit();
            this.tblEmployeeBindingNavigator.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // addressLabel
            // 
            addressLabel.AutoSize = true;
            addressLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            addressLabel.Location = new System.Drawing.Point(54, 130);
            addressLabel.Name = "addressLabel";
            addressLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            addressLabel.Size = new System.Drawing.Size(58, 13);
            addressLabel.TabIndex = 1;
            addressLabel.Text = "Address:";
            addressLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cellLabel
            // 
            cellLabel.AutoSize = true;
            cellLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            cellLabel.Location = new System.Drawing.Point(78, 202);
            cellLabel.Name = "cellLabel";
            cellLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            cellLabel.Size = new System.Drawing.Size(34, 13);
            cellLabel.TabIndex = 3;
            cellLabel.Text = "Cell:";
            cellLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cityLabel
            // 
            cityLabel.AutoSize = true;
            cityLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            cityLabel.Location = new System.Drawing.Point(77, 163);
            cityLabel.Name = "cityLabel";
            cityLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            cityLabel.Size = new System.Drawing.Size(35, 13);
            cityLabel.TabIndex = 5;
            cityLabel.Text = "City:";
            cityLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dOBLabel
            // 
            dOBLabel.AutoSize = true;
            dOBLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dOBLabel.Location = new System.Drawing.Point(23, 96);
            dOBLabel.Name = "dOBLabel";
            dOBLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            dOBLabel.Size = new System.Drawing.Size(89, 13);
            dOBLabel.TabIndex = 9;
            dOBLabel.Text = "Date of Birth :";
            dOBLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // empFirstLabel
            // 
            empFirstLabel.AutoSize = true;
            empFirstLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            empFirstLabel.Location = new System.Drawing.Point(35, 63);
            empFirstLabel.Name = "empFirstLabel";
            empFirstLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            empFirstLabel.Size = new System.Drawing.Size(77, 13);
            empFirstLabel.TabIndex = 11;
            empFirstLabel.Text = "First Name :";
            empFirstLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // empIDLabel
            // 
            empIDLabel.AutoSize = true;
            empIDLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            empIDLabel.Location = new System.Drawing.Point(22, 29);
            empIDLabel.Name = "empIDLabel";
            empIDLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            empIDLabel.Size = new System.Drawing.Size(90, 13);
            empIDLabel.TabIndex = 13;
            empIDLabel.Text = "Employee ID :";
            empIDLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // empLastLabel
            // 
            empLastLabel.AutoSize = true;
            empLastLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            empLastLabel.Location = new System.Drawing.Point(559, 64);
            empLastLabel.Name = "empLastLabel";
            empLastLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            empLastLabel.Size = new System.Drawing.Size(76, 13);
            empLastLabel.TabIndex = 15;
            empLastLabel.Text = "Last Name :";
            empLastLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // empMlLabel
            // 
            empMlLabel.AutoSize = true;
            empMlLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            empMlLabel.Location = new System.Drawing.Point(291, 62);
            empMlLabel.Name = "empMlLabel";
            empMlLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            empMlLabel.Size = new System.Drawing.Size(89, 13);
            empMlLabel.TabIndex = 17;
            empMlLabel.Text = "Middle Name :";
            empMlLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // hourlyRateLabel
            // 
            hourlyRateLabel.AutoSize = true;
            hourlyRateLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            hourlyRateLabel.Location = new System.Drawing.Point(33, 238);
            hourlyRateLabel.Name = "hourlyRateLabel";
            hourlyRateLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            hourlyRateLabel.Size = new System.Drawing.Size(79, 13);
            hourlyRateLabel.TabIndex = 21;
            hourlyRateLabel.Text = "Hourly Rate:";
            hourlyRateLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // phoneLabel
            // 
            phoneLabel.AutoSize = true;
            phoneLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            phoneLabel.Location = new System.Drawing.Point(588, 202);
            phoneLabel.Name = "phoneLabel";
            phoneLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            phoneLabel.Size = new System.Drawing.Size(47, 13);
            phoneLabel.TabIndex = 27;
            phoneLabel.Text = "Phone:";
            phoneLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // postalCodeLabel
            // 
            postalCodeLabel.AutoSize = true;
            postalCodeLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            postalCodeLabel.Location = new System.Drawing.Point(555, 164);
            postalCodeLabel.Name = "postalCodeLabel";
            postalCodeLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            postalCodeLabel.Size = new System.Drawing.Size(80, 13);
            postalCodeLabel.TabIndex = 29;
            postalCodeLabel.Text = "Postal Code:";
            postalCodeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // provinceLabel
            // 
            provinceLabel.AutoSize = true;
            provinceLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            provinceLabel.Location = new System.Drawing.Point(319, 162);
            provinceLabel.Name = "provinceLabel";
            provinceLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            provinceLabel.Size = new System.Drawing.Size(61, 13);
            provinceLabel.TabIndex = 31;
            provinceLabel.Text = "Province:";
            provinceLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // titleLabel
            // 
            titleLabel.AutoSize = true;
            titleLabel.Location = new System.Drawing.Point(599, 30);
            titleLabel.Name = "titleLabel";
            titleLabel.Size = new System.Drawing.Size(36, 13);
            titleLabel.TabIndex = 68;
            titleLabel.Text = "Title:";
            // 
            // commentsLabel
            // 
            commentsLabel.AutoSize = true;
            commentsLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            commentsLabel.Location = new System.Drawing.Point(38, 316);
            commentsLabel.Name = "commentsLabel";
            commentsLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            commentsLabel.Size = new System.Drawing.Size(74, 13);
            commentsLabel.TabIndex = 51;
            commentsLabel.Text = "Comments:";
            commentsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // endDateLabel
            // 
            endDateLabel.AutoSize = true;
            endDateLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            endDateLabel.Location = new System.Drawing.Point(571, 130);
            endDateLabel.Name = "endDateLabel";
            endDateLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            endDateLabel.Size = new System.Drawing.Size(66, 13);
            endDateLabel.TabIndex = 53;
            endDateLabel.Text = "Last Date:";
            endDateLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // memoLabel
            // 
            memoLabel.AutoSize = true;
            memoLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            memoLabel.Location = new System.Drawing.Point(66, 276);
            memoLabel.Name = "memoLabel";
            memoLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            memoLabel.Size = new System.Drawing.Size(46, 13);
            memoLabel.TabIndex = 56;
            memoLabel.Text = "Memo:";
            memoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // reviewLabel
            // 
            reviewLabel.AutoSize = true;
            reviewLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            reviewLabel.Location = new System.Drawing.Point(328, 237);
            reviewLabel.Name = "reviewLabel";
            reviewLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            reviewLabel.Size = new System.Drawing.Size(53, 13);
            reviewLabel.TabIndex = 58;
            reviewLabel.Text = "Review:";
            reviewLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // salaryLabel
            // 
            salaryLabel.AutoSize = true;
            salaryLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            salaryLabel.Location = new System.Drawing.Point(586, 239);
            salaryLabel.Name = "salaryLabel";
            salaryLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            salaryLabel.Size = new System.Drawing.Size(49, 13);
            salaryLabel.TabIndex = 60;
            salaryLabel.Text = "Salary:";
            salaryLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // sINsLabel
            // 
            sINsLabel.AutoSize = true;
            sINsLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            sINsLabel.Location = new System.Drawing.Point(341, 95);
            sINsLabel.Name = "sINsLabel";
            sINsLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            sINsLabel.Size = new System.Drawing.Size(39, 13);
            sINsLabel.TabIndex = 62;
            sINsLabel.Text = "SINs:";
            sINsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // startDateLabel
            // 
            startDateLabel.AutoSize = true;
            startDateLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            startDateLabel.Location = new System.Drawing.Point(564, 97);
            startDateLabel.Name = "startDateLabel";
            startDateLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            startDateLabel.Size = new System.Drawing.Size(73, 13);
            startDateLabel.TabIndex = 64;
            startDateLabel.Text = "Hired Date:";
            startDateLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // addressTextBox
            // 
            this.addressTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblEmployeeBindingSource, "Address", true));
            this.addressTextBox.Location = new System.Drawing.Point(125, 126);
            this.addressTextBox.Name = "addressTextBox";
            this.addressTextBox.Size = new System.Drawing.Size(417, 21);
            this.addressTextBox.TabIndex = 2;
            // 
            // cellTextBox
            // 
            this.cellTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblEmployeeBindingSource, "Cell", true));
            this.cellTextBox.Location = new System.Drawing.Point(125, 198);
            this.cellTextBox.Name = "cellTextBox";
            this.cellTextBox.Size = new System.Drawing.Size(150, 21);
            this.cellTextBox.TabIndex = 4;
            // 
            // cityTextBox
            // 
            this.cityTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblEmployeeBindingSource, "City", true));
            this.cityTextBox.Location = new System.Drawing.Point(125, 161);
            this.cityTextBox.Name = "cityTextBox";
            this.cityTextBox.Size = new System.Drawing.Size(150, 21);
            this.cityTextBox.TabIndex = 6;
            // 
            // dOBDateTimePicker
            // 
            this.dOBDateTimePicker.CustomFormat = " MM / dd / yyyy";
            this.dOBDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.tblEmployeeBindingSource, "DOB", true));
            this.dOBDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dOBDateTimePicker.Location = new System.Drawing.Point(125, 94);
            this.dOBDateTimePicker.Name = "dOBDateTimePicker";
            this.dOBDateTimePicker.Size = new System.Drawing.Size(150, 21);
            this.dOBDateTimePicker.TabIndex = 10;
            // 
            // empFirstTextBox
            // 
            this.empFirstTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblEmployeeBindingSource, "EmpFirst", true));
            this.empFirstTextBox.Location = new System.Drawing.Point(125, 61);
            this.empFirstTextBox.Name = "empFirstTextBox";
            this.empFirstTextBox.Size = new System.Drawing.Size(150, 21);
            this.empFirstTextBox.TabIndex = 12;
            // 
            // empIDTextBox
            // 
            this.empIDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblEmployeeBindingSource, "EmpID", true));
            this.empIDTextBox.Location = new System.Drawing.Point(125, 27);
            this.empIDTextBox.Name = "empIDTextBox";
            this.empIDTextBox.ReadOnly = true;
            this.empIDTextBox.Size = new System.Drawing.Size(150, 21);
            this.empIDTextBox.TabIndex = 14;
            // 
            // empLastTextBox
            // 
            this.empLastTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblEmployeeBindingSource, "EmpLast", true));
            this.empLastTextBox.Location = new System.Drawing.Point(649, 60);
            this.empLastTextBox.Name = "empLastTextBox";
            this.empLastTextBox.Size = new System.Drawing.Size(150, 21);
            this.empLastTextBox.TabIndex = 16;
            // 
            // empMlTextBox
            // 
            this.empMlTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblEmployeeBindingSource, "EmpMl", true));
            this.empMlTextBox.Location = new System.Drawing.Point(392, 58);
            this.empMlTextBox.Name = "empMlTextBox";
            this.empMlTextBox.Size = new System.Drawing.Size(150, 21);
            this.empMlTextBox.TabIndex = 18;
            // 
            // phoneTextBox
            // 
            this.phoneTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblEmployeeBindingSource, "Phone", true));
            this.phoneTextBox.Location = new System.Drawing.Point(649, 198);
            this.phoneTextBox.Name = "phoneTextBox";
            this.phoneTextBox.Size = new System.Drawing.Size(150, 21);
            this.phoneTextBox.TabIndex = 28;
            // 
            // postalCodeTextBox
            // 
            this.postalCodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblEmployeeBindingSource, "PostalCode", true));
            this.postalCodeTextBox.Location = new System.Drawing.Point(649, 160);
            this.postalCodeTextBox.Name = "postalCodeTextBox";
            this.postalCodeTextBox.Size = new System.Drawing.Size(150, 21);
            this.postalCodeTextBox.TabIndex = 30;
            // 
            // provinceComboBox
            // 
            this.provinceComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblEmployeeBindingSource, "Province", true));
            this.provinceComboBox.FormattingEnabled = true;
            this.provinceComboBox.Items.AddRange(new object[] {
            "QC",
            "NS",
            "PE",
            "NL",
            "NB",
            "MB",
            "SK",
            "AB",
            "BC",
            "YT",
            "NT",
            "NU"});
            this.provinceComboBox.Location = new System.Drawing.Point(392, 158);
            this.provinceComboBox.Name = "provinceComboBox";
            this.provinceComboBox.Size = new System.Drawing.Size(150, 21);
            this.provinceComboBox.TabIndex = 49;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.titleComboBox);
            this.groupBox1.Controls.Add(titleLabel);
            this.groupBox1.Controls.Add(commentsLabel);
            this.groupBox1.Controls.Add(this.commentsTextBox);
            this.groupBox1.Controls.Add(endDateLabel);
            this.groupBox1.Controls.Add(this.endDateDateTimePicker);
            this.groupBox1.Controls.Add(this.hourlyRateTextBox);
            this.groupBox1.Controls.Add(memoLabel);
            this.groupBox1.Controls.Add(this.memoTextBox);
            this.groupBox1.Controls.Add(reviewLabel);
            this.groupBox1.Controls.Add(this.reviewDateTimePicker);
            this.groupBox1.Controls.Add(salaryLabel);
            this.groupBox1.Controls.Add(this.salaryTextBox);
            this.groupBox1.Controls.Add(sINsLabel);
            this.groupBox1.Controls.Add(this.sINsTextBox);
            this.groupBox1.Controls.Add(startDateLabel);
            this.groupBox1.Controls.Add(this.startDateDateTimePicker);
            this.groupBox1.Controls.Add(this.empIDTextBox);
            this.groupBox1.Controls.Add(provinceLabel);
            this.groupBox1.Controls.Add(this.provinceComboBox);
            this.groupBox1.Controls.Add(this.postalCodeTextBox);
            this.groupBox1.Controls.Add(postalCodeLabel);
            this.groupBox1.Controls.Add(this.phoneTextBox);
            this.groupBox1.Controls.Add(phoneLabel);
            this.groupBox1.Controls.Add(hourlyRateLabel);
            this.groupBox1.Controls.Add(this.empMlTextBox);
            this.groupBox1.Controls.Add(addressLabel);
            this.groupBox1.Controls.Add(empMlLabel);
            this.groupBox1.Controls.Add(this.addressTextBox);
            this.groupBox1.Controls.Add(this.empLastTextBox);
            this.groupBox1.Controls.Add(cellLabel);
            this.groupBox1.Controls.Add(empLastLabel);
            this.groupBox1.Controls.Add(this.cellTextBox);
            this.groupBox1.Controls.Add(empIDLabel);
            this.groupBox1.Controls.Add(cityLabel);
            this.groupBox1.Controls.Add(this.empFirstTextBox);
            this.groupBox1.Controls.Add(this.cityTextBox);
            this.groupBox1.Controls.Add(empFirstLabel);
            this.groupBox1.Controls.Add(this.dOBDateTimePicker);
            this.groupBox1.Controls.Add(dOBLabel);
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.groupBox1.Location = new System.Drawing.Point(-7, 69);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(825, 426);
            this.groupBox1.TabIndex = 51;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "EMPLOYEE INFORMARION";
            // 
            // titleComboBox
            // 
            this.titleComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblEmployeeBindingSource, "tblJobTitle.Title", true));
            this.titleComboBox.FormattingEnabled = true;
            this.titleComboBox.Location = new System.Drawing.Point(649, 26);
            this.titleComboBox.Name = "titleComboBox";
            this.titleComboBox.Size = new System.Drawing.Size(150, 21);
            this.titleComboBox.TabIndex = 69;
            // 
            // commentsTextBox
            // 
            this.commentsTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblEmployeeBindingSource, "Comments", true));
            this.commentsTextBox.Location = new System.Drawing.Point(125, 313);
            this.commentsTextBox.Multiline = true;
            this.commentsTextBox.Name = "commentsTextBox";
            this.commentsTextBox.Size = new System.Drawing.Size(674, 95);
            this.commentsTextBox.TabIndex = 52;
            // 
            // endDateDateTimePicker
            // 
            this.endDateDateTimePicker.CustomFormat = " MM / dd / yyyy";
            this.endDateDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.tblEmployeeBindingSource, "EndDate", true));
            this.endDateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.endDateDateTimePicker.Location = new System.Drawing.Point(649, 126);
            this.endDateDateTimePicker.Name = "endDateDateTimePicker";
            this.endDateDateTimePicker.Size = new System.Drawing.Size(150, 21);
            this.endDateDateTimePicker.TabIndex = 54;
            // 
            // hourlyRateTextBox
            // 
            this.hourlyRateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblEmployeeBindingSource, "HourlyRate", true));
            this.hourlyRateTextBox.Location = new System.Drawing.Point(125, 236);
            this.hourlyRateTextBox.Name = "hourlyRateTextBox";
            this.hourlyRateTextBox.Size = new System.Drawing.Size(150, 21);
            this.hourlyRateTextBox.TabIndex = 55;
            // 
            // memoTextBox
            // 
            this.memoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblEmployeeBindingSource, "Memo", true));
            this.memoTextBox.Location = new System.Drawing.Point(125, 274);
            this.memoTextBox.Name = "memoTextBox";
            this.memoTextBox.Size = new System.Drawing.Size(674, 21);
            this.memoTextBox.TabIndex = 57;
            // 
            // reviewDateTimePicker
            // 
            this.reviewDateTimePicker.CustomFormat = " MM / dd / yyyy";
            this.reviewDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.tblEmployeeBindingSource, "Review", true));
            this.reviewDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.reviewDateTimePicker.Location = new System.Drawing.Point(392, 233);
            this.reviewDateTimePicker.Name = "reviewDateTimePicker";
            this.reviewDateTimePicker.Size = new System.Drawing.Size(150, 21);
            this.reviewDateTimePicker.TabIndex = 59;
            // 
            // salaryTextBox
            // 
            this.salaryTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblEmployeeBindingSource, "Salary", true));
            this.salaryTextBox.Location = new System.Drawing.Point(649, 235);
            this.salaryTextBox.Name = "salaryTextBox";
            this.salaryTextBox.Size = new System.Drawing.Size(150, 21);
            this.salaryTextBox.TabIndex = 61;
            // 
            // sINsTextBox
            // 
            this.sINsTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblEmployeeBindingSource, "SINs", true));
            this.sINsTextBox.Location = new System.Drawing.Point(392, 91);
            this.sINsTextBox.Name = "sINsTextBox";
            this.sINsTextBox.Size = new System.Drawing.Size(150, 21);
            this.sINsTextBox.TabIndex = 63;
            // 
            // startDateDateTimePicker
            // 
            this.startDateDateTimePicker.CustomFormat = " MM / dd / yyyy";
            this.startDateDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.tblEmployeeBindingSource, "StartDate", true));
            this.startDateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.startDateDateTimePicker.Location = new System.Drawing.Point(649, 93);
            this.startDateDateTimePicker.Name = "startDateDateTimePicker";
            this.startDateDateTimePicker.Size = new System.Drawing.Size(150, 21);
            this.startDateDateTimePicker.TabIndex = 65;
            // 
            // tblEmployeeBindingSource
            // 
            this.tblEmployeeBindingSource.DataSource = typeof(AragonPharmacyLibrary.tblEmployee);
            // 
            // Panel1
            // 
            this.Panel1.BackColor = System.Drawing.Color.White;
            this.Panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Panel1.BackgroundImage")));
            this.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel1.Controls.Add(this.lblTitle);
            this.Panel1.Location = new System.Drawing.Point(-7, 0);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(824, 60);
            this.Panel1.TabIndex = 45;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.BackColor = System.Drawing.Color.White;
            this.lblTitle.Font = new System.Drawing.Font("Impact", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.SteelBlue;
            this.lblTitle.Location = new System.Drawing.Point(10, 13);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(226, 34);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Add | Edit Employee";
            // 
            // lblSearchLastName
            // 
            this.lblSearchLastName.AutoSize = true;
            this.lblSearchLastName.Location = new System.Drawing.Point(305, 18);
            this.lblSearchLastName.Name = "lblSearchLastName";
            this.lblSearchLastName.Size = new System.Drawing.Size(138, 13);
            this.lblSearchLastName.TabIndex = 0;
            this.lblSearchLastName.Text = "Search by Last Name :";
            // 
            // btnFind
            // 
            this.btnFind.Location = new System.Drawing.Point(630, 17);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(75, 23);
            this.btnFind.TabIndex = 52;
            this.btnFind.Text = "&Find";
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // btnFindAll
            // 
            this.btnFindAll.Location = new System.Drawing.Point(719, 17);
            this.btnFindAll.Name = "btnFindAll";
            this.btnFindAll.Size = new System.Drawing.Size(75, 23);
            this.btnFindAll.TabIndex = 53;
            this.btnFindAll.Text = "Find &All";
            this.btnFindAll.UseVisualStyleBackColor = true;
            this.btnFindAll.Click += new System.EventHandler(this.btnFindAll_Click);
            // 
            // tblEmployeeBindingNavigator
            // 
            this.tblEmployeeBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.tblEmployeeBindingNavigator.BindingSource = this.tblEmployeeBindingSource;
            this.tblEmployeeBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.tblEmployeeBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.tblEmployeeBindingNavigator.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tblEmployeeBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.tblEmployeeBindingNavigatorSaveItem,
            this.ToolStripSeparator1});
            this.tblEmployeeBindingNavigator.Location = new System.Drawing.Point(3, 15);
            this.tblEmployeeBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.tblEmployeeBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.tblEmployeeBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.tblEmployeeBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.tblEmployeeBindingNavigator.Name = "tblEmployeeBindingNavigator";
            this.tblEmployeeBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.tblEmployeeBindingNavigator.Size = new System.Drawing.Size(807, 25);
            this.tblEmployeeBindingNavigator.TabIndex = 54;
            this.tblEmployeeBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(58, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // tblEmployeeBindingNavigatorSaveItem
            // 
            this.tblEmployeeBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tblEmployeeBindingNavigatorSaveItem.Enabled = false;
            this.tblEmployeeBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("tblEmployeeBindingNavigatorSaveItem.Image")));
            this.tblEmployeeBindingNavigatorSaveItem.Name = "tblEmployeeBindingNavigatorSaveItem";
            this.tblEmployeeBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.tblEmployeeBindingNavigatorSaveItem.Text = "Save Data";
            this.tblEmployeeBindingNavigatorSaveItem.Click += new System.EventHandler(this.tblEmployeeBindingNavigatorSaveItem_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnFindAll);
            this.groupBox3.Controls.Add(this.btnFind);
            this.groupBox3.Controls.Add(this.txtSearchLastName);
            this.groupBox3.Controls.Add(this.lblSearchLastName);
            this.groupBox3.Controls.Add(this.tblEmployeeBindingNavigator);
            this.groupBox3.Location = new System.Drawing.Point(-1, 492);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(813, 43);
            this.groupBox3.TabIndex = 54;
            this.groupBox3.TabStop = false;
            // 
            // txtSearchLastName
            // 
            this.txtSearchLastName.Location = new System.Drawing.Point(449, 17);
            this.txtSearchLastName.Name = "txtSearchLastName";
            this.txtSearchLastName.Size = new System.Drawing.Size(165, 21);
            this.txtSearchLastName.TabIndex = 55;
            // 
            // frmEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(812, 537);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Panel1);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmEmployee";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "Employee";
            this.Load += new System.EventHandler(this.frmEmployee_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblEmployeeBindingSource)).EndInit();
            this.Panel1.ResumeLayout(false);
            this.Panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblEmployeeBindingNavigator)).EndInit();
            this.tblEmployeeBindingNavigator.ResumeLayout(false);
            this.tblEmployeeBindingNavigator.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource tblEmployeeBindingSource;
        private System.Windows.Forms.TextBox addressTextBox;
        private System.Windows.Forms.TextBox cellTextBox;
        private System.Windows.Forms.TextBox cityTextBox;
        private System.Windows.Forms.DateTimePicker dOBDateTimePicker;
        private System.Windows.Forms.TextBox empFirstTextBox;
        private System.Windows.Forms.TextBox empIDTextBox;
        private System.Windows.Forms.TextBox empLastTextBox;
        private System.Windows.Forms.TextBox empMlTextBox;
        private System.Windows.Forms.TextBox phoneTextBox;
        private System.Windows.Forms.TextBox postalCodeTextBox;
        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.ComboBox provinceComboBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox titleComboBox;
        private System.Windows.Forms.TextBox commentsTextBox;
        private System.Windows.Forms.DateTimePicker endDateDateTimePicker;
        private System.Windows.Forms.TextBox hourlyRateTextBox;
        private System.Windows.Forms.TextBox memoTextBox;
        private System.Windows.Forms.DateTimePicker reviewDateTimePicker;
        private System.Windows.Forms.TextBox salaryTextBox;
        private System.Windows.Forms.TextBox sINsTextBox;
        private System.Windows.Forms.DateTimePicker startDateDateTimePicker;
        private System.Windows.Forms.Label lblSearchLastName;
        private System.Windows.Forms.Button btnFind;
        private System.Windows.Forms.Button btnFindAll;
        private System.Windows.Forms.BindingNavigator tblEmployeeBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton tblEmployeeBindingNavigatorSaveItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtSearchLastName;
    }
}