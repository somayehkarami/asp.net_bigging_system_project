﻿
namespace AragonPharmacyApp
{
    partial class frmReport1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtOutputResult = new System.Windows.Forms.TextBox();
            this.btnDrugCost = new System.Windows.Forms.Button();
            this.btnRxCustomers = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtOutputResult
            // 
            this.txtOutputResult.BackColor = System.Drawing.SystemColors.Info;
            this.txtOutputResult.Location = new System.Drawing.Point(152, 5);
            this.txtOutputResult.Multiline = true;
            this.txtOutputResult.Name = "txtOutputResult";
            this.txtOutputResult.ReadOnly = true;
            this.txtOutputResult.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtOutputResult.Size = new System.Drawing.Size(636, 435);
            this.txtOutputResult.TabIndex = 2;
            // 
            // btnDrugCost
            // 
            this.btnDrugCost.BackColor = System.Drawing.SystemColors.Info;
            this.btnDrugCost.Location = new System.Drawing.Point(12, 28);
            this.btnDrugCost.Name = "btnDrugCost";
            this.btnDrugCost.Size = new System.Drawing.Size(108, 36);
            this.btnDrugCost.TabIndex = 3;
            this.btnDrugCost.Text = "Drug and Cost";
            this.btnDrugCost.UseVisualStyleBackColor = false;
            this.btnDrugCost.Click += new System.EventHandler(this.btnDrugCost_Click);
            // 
            // btnRxCustomers
            // 
            this.btnRxCustomers.BackColor = System.Drawing.SystemColors.Info;
            this.btnRxCustomers.Location = new System.Drawing.Point(11, 87);
            this.btnRxCustomers.Name = "btnRxCustomers";
            this.btnRxCustomers.Size = new System.Drawing.Size(109, 36);
            this.btnRxCustomers.TabIndex = 4;
            this.btnRxCustomers.Text = "Rx and Customer";
            this.btnRxCustomers.UseVisualStyleBackColor = false;
            this.btnRxCustomers.Click += new System.EventHandler(this.btnRxCustomers_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackgroundImage = global::AragonPharmacyApp.Properties.Resources.exit;
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnExit.Font = new System.Drawing.Font("Verdana", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(37, 393);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(49, 47);
            this.btnExit.TabIndex = 13;
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnClear
            // 
            this.btnClear.BackgroundImage = global::AragonPharmacyApp.Properties.Resources.eraser;
            this.btnClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClear.Font = new System.Drawing.Font("Verdana", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.Location = new System.Drawing.Point(37, 314);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(49, 46);
            this.btnClear.TabIndex = 17;
            this.btnClear.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // frmReport1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 452);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnRxCustomers);
            this.Controls.Add(this.btnDrugCost);
            this.Controls.Add(this.txtOutputResult);
            this.Name = "frmReport1";
            this.Text = "Report1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtOutputResult;
        private System.Windows.Forms.Button btnDrugCost;
        private System.Windows.Forms.Button btnRxCustomers;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnClear;
    }
}