﻿
namespace AragonPharmacyApp
{
    partial class frmPrescription
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label autoRefillsLabel;
            System.Windows.Forms.Label custIDLabel;
            System.Windows.Forms.Label dateLabel;
            System.Windows.Forms.Label dINLabel;
            System.Windows.Forms.Label doctorIDLabel;
            System.Windows.Forms.Label expireDateLabel;
            System.Windows.Forms.Label instructionsLabel;
            System.Windows.Forms.Label prescriptionIDLabel;
            System.Windows.Forms.Label quantityLabel;
            System.Windows.Forms.Label refillsLabel;
            System.Windows.Forms.Label refillUsedLabel;
            System.Windows.Forms.Label unitLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrescription));
            this.tblRxBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.refillsTextBox = new System.Windows.Forms.TextBox();
            this.autoRefillsCheckBox = new System.Windows.Forms.CheckBox();
            this.custIDTextBox = new System.Windows.Forms.TextBox();
            this.dateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.dINTextBox = new System.Windows.Forms.TextBox();
            this.doctorIDTextBox = new System.Windows.Forms.TextBox();
            this.expireDateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.instructionsTextBox = new System.Windows.Forms.TextBox();
            this.prescriptionIDTextBox = new System.Windows.Forms.TextBox();
            this.quantityTextBox = new System.Windows.Forms.TextBox();
            this.refillUsedTextBox = new System.Windows.Forms.TextBox();
            this.unitTextBox = new System.Windows.Forms.TextBox();
            this.btnFind = new System.Windows.Forms.Button();
            this.txtSearchID = new System.Windows.Forms.TextBox();
            this.lblSearchLastName = new System.Windows.Forms.Label();
            this.btnFindAll = new System.Windows.Forms.Button();
            this.tblRxBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.tblRxBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.printToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.helpToolStripButton = new System.Windows.Forms.ToolStripButton();
            autoRefillsLabel = new System.Windows.Forms.Label();
            custIDLabel = new System.Windows.Forms.Label();
            dateLabel = new System.Windows.Forms.Label();
            dINLabel = new System.Windows.Forms.Label();
            doctorIDLabel = new System.Windows.Forms.Label();
            expireDateLabel = new System.Windows.Forms.Label();
            instructionsLabel = new System.Windows.Forms.Label();
            prescriptionIDLabel = new System.Windows.Forms.Label();
            quantityLabel = new System.Windows.Forms.Label();
            refillsLabel = new System.Windows.Forms.Label();
            refillUsedLabel = new System.Windows.Forms.Label();
            unitLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.tblRxBindingNavigator)).BeginInit();
            this.tblRxBindingNavigator.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblRxBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // autoRefillsLabel
            // 
            autoRefillsLabel.AutoSize = true;
            autoRefillsLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            autoRefillsLabel.Location = new System.Drawing.Point(324, 102);
            autoRefillsLabel.Name = "autoRefillsLabel";
            autoRefillsLabel.Size = new System.Drawing.Size(76, 13);
            autoRefillsLabel.TabIndex = 25;
            autoRefillsLabel.Text = "Auto Refills:";
            // 
            // custIDLabel
            // 
            custIDLabel.AutoSize = true;
            custIDLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            custIDLabel.Location = new System.Drawing.Point(47, 61);
            custIDLabel.Name = "custIDLabel";
            custIDLabel.Size = new System.Drawing.Size(86, 13);
            custIDLabel.TabIndex = 27;
            custIDLabel.Text = "Customer ID:";
            // 
            // dateLabel
            // 
            dateLabel.AutoSize = true;
            dateLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dateLabel.Location = new System.Drawing.Point(361, 27);
            dateLabel.Name = "dateLabel";
            dateLabel.Size = new System.Drawing.Size(39, 13);
            dateLabel.TabIndex = 29;
            dateLabel.Text = "Date:";
            // 
            // dINLabel
            // 
            dINLabel.AutoSize = true;
            dINLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dINLabel.Location = new System.Drawing.Point(99, 102);
            dINLabel.Name = "dINLabel";
            dINLabel.Size = new System.Drawing.Size(34, 13);
            dINLabel.TabIndex = 31;
            dINLabel.Text = "DIN:";
            // 
            // doctorIDLabel
            // 
            doctorIDLabel.AutoSize = true;
            doctorIDLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            doctorIDLabel.Location = new System.Drawing.Point(332, 65);
            doctorIDLabel.Name = "doctorIDLabel";
            doctorIDLabel.Size = new System.Drawing.Size(68, 13);
            doctorIDLabel.TabIndex = 33;
            doctorIDLabel.Text = "Doctor ID:";
            // 
            // expireDateLabel
            // 
            expireDateLabel.AutoSize = true;
            expireDateLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            expireDateLabel.Location = new System.Drawing.Point(321, 137);
            expireDateLabel.Name = "expireDateLabel";
            expireDateLabel.Size = new System.Drawing.Size(79, 13);
            expireDateLabel.TabIndex = 35;
            expireDateLabel.Text = "Expire Date:";
            // 
            // instructionsLabel
            // 
            instructionsLabel.AutoSize = true;
            instructionsLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            instructionsLabel.Location = new System.Drawing.Point(54, 137);
            instructionsLabel.Name = "instructionsLabel";
            instructionsLabel.Size = new System.Drawing.Size(79, 13);
            instructionsLabel.TabIndex = 37;
            instructionsLabel.Text = "Instructions:";
            // 
            // prescriptionIDLabel
            // 
            prescriptionIDLabel.AutoSize = true;
            prescriptionIDLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            prescriptionIDLabel.Location = new System.Drawing.Point(36, 27);
            prescriptionIDLabel.Name = "prescriptionIDLabel";
            prescriptionIDLabel.Size = new System.Drawing.Size(97, 13);
            prescriptionIDLabel.TabIndex = 39;
            prescriptionIDLabel.Text = "Prescription ID:";
            // 
            // quantityLabel
            // 
            quantityLabel.AutoSize = true;
            quantityLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            quantityLabel.Location = new System.Drawing.Point(73, 175);
            quantityLabel.Name = "quantityLabel";
            quantityLabel.Size = new System.Drawing.Size(60, 13);
            quantityLabel.TabIndex = 41;
            quantityLabel.Text = "Quantity:";
            // 
            // refillsLabel
            // 
            refillsLabel.AutoSize = true;
            refillsLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            refillsLabel.Location = new System.Drawing.Point(354, 206);
            refillsLabel.Name = "refillsLabel";
            refillsLabel.Size = new System.Drawing.Size(46, 13);
            refillsLabel.TabIndex = 43;
            refillsLabel.Text = "Refills:";
            // 
            // refillUsedLabel
            // 
            refillUsedLabel.AutoSize = true;
            refillUsedLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            refillUsedLabel.Location = new System.Drawing.Point(61, 210);
            refillUsedLabel.Name = "refillUsedLabel";
            refillUsedLabel.Size = new System.Drawing.Size(72, 13);
            refillUsedLabel.TabIndex = 45;
            refillUsedLabel.Text = "Refill Used:";
            // 
            // unitLabel
            // 
            unitLabel.AutoSize = true;
            unitLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            unitLabel.Location = new System.Drawing.Point(366, 175);
            unitLabel.Name = "unitLabel";
            unitLabel.Size = new System.Drawing.Size(34, 13);
            unitLabel.TabIndex = 47;
            unitLabel.Text = "Unit:";
            // 
            // tblRxBindingNavigator
            // 
            this.tblRxBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.tblRxBindingNavigator.BindingSource = this.tblRxBindingSource;
            this.tblRxBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.tblRxBindingNavigator.DeleteItem = null;
            this.tblRxBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.tblRxBindingNavigatorSaveItem,
            this.printToolStripButton,
            this.toolStripSeparator,
            this.helpToolStripButton});
            this.tblRxBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.tblRxBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.tblRxBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.tblRxBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.tblRxBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.tblRxBindingNavigator.Name = "tblRxBindingNavigator";
            this.tblRxBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.tblRxBindingNavigator.Size = new System.Drawing.Size(650, 25);
            this.tblRxBindingNavigator.TabIndex = 0;
            this.tblRxBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.refillsTextBox);
            this.groupBox1.Controls.Add(autoRefillsLabel);
            this.groupBox1.Controls.Add(this.autoRefillsCheckBox);
            this.groupBox1.Controls.Add(custIDLabel);
            this.groupBox1.Controls.Add(this.custIDTextBox);
            this.groupBox1.Controls.Add(dateLabel);
            this.groupBox1.Controls.Add(this.dateDateTimePicker);
            this.groupBox1.Controls.Add(dINLabel);
            this.groupBox1.Controls.Add(this.dINTextBox);
            this.groupBox1.Controls.Add(doctorIDLabel);
            this.groupBox1.Controls.Add(this.doctorIDTextBox);
            this.groupBox1.Controls.Add(expireDateLabel);
            this.groupBox1.Controls.Add(this.expireDateDateTimePicker);
            this.groupBox1.Controls.Add(instructionsLabel);
            this.groupBox1.Controls.Add(this.instructionsTextBox);
            this.groupBox1.Controls.Add(prescriptionIDLabel);
            this.groupBox1.Controls.Add(this.prescriptionIDTextBox);
            this.groupBox1.Controls.Add(quantityLabel);
            this.groupBox1.Controls.Add(this.quantityTextBox);
            this.groupBox1.Controls.Add(refillsLabel);
            this.groupBox1.Controls.Add(refillUsedLabel);
            this.groupBox1.Controls.Add(this.refillUsedTextBox);
            this.groupBox1.Controls.Add(unitLabel);
            this.groupBox1.Controls.Add(this.unitTextBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 28);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(626, 268);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "PRESCRIPTION";
            // 
            // refillsTextBox
            // 
            this.refillsTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblRxBindingSource, "Refills", true));
            this.refillsTextBox.Location = new System.Drawing.Point(421, 203);
            this.refillsTextBox.Name = "refillsTextBox";
            this.refillsTextBox.Size = new System.Drawing.Size(150, 20);
            this.refillsTextBox.TabIndex = 49;
            // 
            // autoRefillsCheckBox
            // 
            this.autoRefillsCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.tblRxBindingSource, "AutoRefills", true));
            this.autoRefillsCheckBox.Location = new System.Drawing.Point(421, 96);
            this.autoRefillsCheckBox.Name = "autoRefillsCheckBox";
            this.autoRefillsCheckBox.Size = new System.Drawing.Size(20, 24);
            this.autoRefillsCheckBox.TabIndex = 26;
            this.autoRefillsCheckBox.UseVisualStyleBackColor = true;
            // 
            // custIDTextBox
            // 
            this.custIDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblRxBindingSource, "CustID", true));
            this.custIDTextBox.Location = new System.Drawing.Point(152, 61);
            this.custIDTextBox.Name = "custIDTextBox";
            this.custIDTextBox.Size = new System.Drawing.Size(150, 20);
            this.custIDTextBox.TabIndex = 28;
            // 
            // dateDateTimePicker
            // 
            this.dateDateTimePicker.CustomFormat = " MM / dd / yyyy";
            this.dateDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.tblRxBindingSource, "Date", true));
            this.dateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateDateTimePicker.Location = new System.Drawing.Point(421, 23);
            this.dateDateTimePicker.Name = "dateDateTimePicker";
            this.dateDateTimePicker.Size = new System.Drawing.Size(150, 20);
            this.dateDateTimePicker.TabIndex = 30;
            // 
            // dINTextBox
            // 
            this.dINTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblRxBindingSource, "DIN", true));
            this.dINTextBox.Location = new System.Drawing.Point(152, 98);
            this.dINTextBox.Name = "dINTextBox";
            this.dINTextBox.Size = new System.Drawing.Size(150, 20);
            this.dINTextBox.TabIndex = 32;
            // 
            // doctorIDTextBox
            // 
            this.doctorIDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblRxBindingSource, "DoctorID", true));
            this.doctorIDTextBox.Location = new System.Drawing.Point(421, 61);
            this.doctorIDTextBox.Name = "doctorIDTextBox";
            this.doctorIDTextBox.Size = new System.Drawing.Size(150, 20);
            this.doctorIDTextBox.TabIndex = 34;
            // 
            // expireDateDateTimePicker
            // 
            this.expireDateDateTimePicker.CustomFormat = " MM / dd / yyyy";
            this.expireDateDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.tblRxBindingSource, "ExpireDate", true));
            this.expireDateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.expireDateDateTimePicker.Location = new System.Drawing.Point(421, 133);
            this.expireDateDateTimePicker.Name = "expireDateDateTimePicker";
            this.expireDateDateTimePicker.Size = new System.Drawing.Size(150, 20);
            this.expireDateDateTimePicker.TabIndex = 36;
            // 
            // instructionsTextBox
            // 
            this.instructionsTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblRxBindingSource, "Instructions", true));
            this.instructionsTextBox.Location = new System.Drawing.Point(152, 133);
            this.instructionsTextBox.Name = "instructionsTextBox";
            this.instructionsTextBox.Size = new System.Drawing.Size(150, 20);
            this.instructionsTextBox.TabIndex = 38;
            // 
            // prescriptionIDTextBox
            // 
            this.prescriptionIDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblRxBindingSource, "PrescriptionID", true));
            this.prescriptionIDTextBox.Location = new System.Drawing.Point(152, 23);
            this.prescriptionIDTextBox.Name = "prescriptionIDTextBox";
            this.prescriptionIDTextBox.Size = new System.Drawing.Size(150, 20);
            this.prescriptionIDTextBox.TabIndex = 40;
            // 
            // quantityTextBox
            // 
            this.quantityTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblRxBindingSource, "Quantity", true));
            this.quantityTextBox.Location = new System.Drawing.Point(152, 171);
            this.quantityTextBox.Name = "quantityTextBox";
            this.quantityTextBox.Size = new System.Drawing.Size(150, 20);
            this.quantityTextBox.TabIndex = 42;
            // 
            // refillUsedTextBox
            // 
            this.refillUsedTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblRxBindingSource, "RefillUsed", true));
            this.refillUsedTextBox.Location = new System.Drawing.Point(152, 206);
            this.refillUsedTextBox.Name = "refillUsedTextBox";
            this.refillUsedTextBox.Size = new System.Drawing.Size(150, 20);
            this.refillUsedTextBox.TabIndex = 46;
            // 
            // unitTextBox
            // 
            this.unitTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblRxBindingSource, "Unit", true));
            this.unitTextBox.Location = new System.Drawing.Point(421, 171);
            this.unitTextBox.Name = "unitTextBox";
            this.unitTextBox.Size = new System.Drawing.Size(150, 20);
            this.unitTextBox.TabIndex = 48;
            // 
            // btnFind
            // 
            this.btnFind.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnFind.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFind.Location = new System.Drawing.Point(381, 318);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(75, 23);
            this.btnFind.TabIndex = 57;
            this.btnFind.Text = "&Find";
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // txtSearchID
            // 
            this.txtSearchID.Location = new System.Drawing.Point(164, 319);
            this.txtSearchID.Name = "txtSearchID";
            this.txtSearchID.Size = new System.Drawing.Size(150, 20);
            this.txtSearchID.TabIndex = 58;
            // 
            // lblSearchLastName
            // 
            this.lblSearchLastName.AutoSize = true;
            this.lblSearchLastName.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchLastName.Location = new System.Drawing.Point(53, 323);
            this.lblSearchLastName.Name = "lblSearchLastName";
            this.lblSearchLastName.Size = new System.Drawing.Size(92, 13);
            this.lblSearchLastName.TabIndex = 56;
            this.lblSearchLastName.Text = "Search by ID :";
            // 
            // btnFindAll
            // 
            this.btnFindAll.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnFindAll.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFindAll.Location = new System.Drawing.Point(491, 318);
            this.btnFindAll.Name = "btnFindAll";
            this.btnFindAll.Size = new System.Drawing.Size(75, 23);
            this.btnFindAll.TabIndex = 59;
            this.btnFindAll.Text = "Search &All";
            this.btnFindAll.UseVisualStyleBackColor = true;
            this.btnFindAll.Click += new System.EventHandler(this.btnFindAll_Click);
            // 
            // tblRxBindingSource
            // 
            this.tblRxBindingSource.DataSource = typeof(AragonPharmacyLibrary.tblRx);
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // tblRxBindingNavigatorSaveItem
            // 
            this.tblRxBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tblRxBindingNavigatorSaveItem.Enabled = false;
            this.tblRxBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("tblRxBindingNavigatorSaveItem.Image")));
            this.tblRxBindingNavigatorSaveItem.Name = "tblRxBindingNavigatorSaveItem";
            this.tblRxBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.tblRxBindingNavigatorSaveItem.Text = "Save Data";
            this.tblRxBindingNavigatorSaveItem.Click += new System.EventHandler(this.tblRxBindingNavigatorSaveItem_Click);
            // 
            // printToolStripButton
            // 
            this.printToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.printToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("printToolStripButton.Image")));
            this.printToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printToolStripButton.Name = "printToolStripButton";
            this.printToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.printToolStripButton.Text = "&Print";
            // 
            // helpToolStripButton
            // 
            this.helpToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.helpToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("helpToolStripButton.Image")));
            this.helpToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.helpToolStripButton.Name = "helpToolStripButton";
            this.helpToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.helpToolStripButton.Text = "He&lp";
            // 
            // frmPrescription
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.Info;
            this.ClientSize = new System.Drawing.Size(650, 375);
            this.Controls.Add(this.btnFindAll);
            this.Controls.Add(this.btnFind);
            this.Controls.Add(this.txtSearchID);
            this.Controls.Add(this.lblSearchLastName);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tblRxBindingNavigator);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmPrescription";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Prescription";
            this.Load += new System.EventHandler(this.frmPrescription_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tblRxBindingNavigator)).EndInit();
            this.tblRxBindingNavigator.ResumeLayout(false);
            this.tblRxBindingNavigator.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblRxBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource tblRxBindingSource;
        private System.Windows.Forms.BindingNavigator tblRxBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton tblRxBindingNavigatorSaveItem;
        private System.Windows.Forms.ToolStripButton printToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripButton helpToolStripButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox autoRefillsCheckBox;
        private System.Windows.Forms.TextBox custIDTextBox;
        private System.Windows.Forms.DateTimePicker dateDateTimePicker;
        private System.Windows.Forms.TextBox dINTextBox;
        private System.Windows.Forms.TextBox doctorIDTextBox;
        private System.Windows.Forms.DateTimePicker expireDateDateTimePicker;
        private System.Windows.Forms.TextBox instructionsTextBox;
        private System.Windows.Forms.TextBox prescriptionIDTextBox;
        private System.Windows.Forms.TextBox quantityTextBox;
        private System.Windows.Forms.TextBox refillUsedTextBox;
        private System.Windows.Forms.TextBox unitTextBox;
        private System.Windows.Forms.TextBox refillsTextBox;
        private System.Windows.Forms.Button btnFind;
        private System.Windows.Forms.TextBox txtSearchID;
        private System.Windows.Forms.Label lblSearchLastName;
        private System.Windows.Forms.Button btnFindAll;
    }
}