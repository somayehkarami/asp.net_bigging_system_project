﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AragonPharmacyApp
{
    public partial class frmPrescription : Form
    {
        public frmPrescription()
        {
            InitializeComponent();
        }
        private AragonPharmacyLibrary.AragonPharmacyDBEntities dbcontext = null;

        // fill our tblRxBindingSource with all rows, ordered by last
        // name first, then by first name
        private void RefreshPrescriptions()
        {
            // Dispose old dbContext, if any
            if (dbcontext != null)
            {
                dbcontext.Dispose();
            }

            // create a new dbContext so we can reorder records based on edits
            dbcontext = new AragonPharmacyLibrary.AragonPharmacyDBEntities();

            // use LINQ to order 
            // by last name, then first name
            dbcontext.tblRxes
                .OrderBy(prescription => prescription.PrescriptionID)
                .Load();

            // specify DataSource for tblRxBindingSource
           tblRxBindingSource.DataSource = dbcontext.tblRxes.Local;

            tblRxBindingSource.MoveFirst(); // go to first record

           
        }

        private void frmPrescription_Load(object sender, EventArgs e)
        {
            // when the form loads, fill it with data from the database
            RefreshPrescriptions(); // fill binding with data from database

            // enable the Save button
            tblRxBindingNavigatorSaveItem.Enabled = true;

            txtSearchID.Clear(); // clear the find textbox
        }

       

        private void tblRxBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            // click event handler for the Save buttonn in the 
            // BindingNavigator saves the changes made to the data 
            Validate(); // validate input fields
            tblRxBindingSource.EndEdit(); // complete current edit, if any

            // try to save changes
            try
            {
                dbcontext.SaveChanges();  // write changes to the database
            }
            catch (DbEntityValidationException)
            {
                MessageBox.Show("Columns cannot be empty", "Entity Validation Exception");
            }

            RefreshPrescriptions(); // change back to initial unfiltered data
        }

        private void btnFindAll_Click(object sender, EventArgs e)
        {
            bindingNavigatorAddNewItem.Enabled = true;
            

            RefreshPrescriptions(); // change back to initial unfiltered data
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            var lastNameQuery =
               from prescription in dbcontext.tblRxes
               where   prescription.PrescriptionID.StartsWith(txtSearchID.Text)
               orderby prescription.PrescriptionID
               select prescription;

            // display matching contacts
            tblRxBindingSource.DataSource = lastNameQuery.ToList();
            tblRxBindingSource.MoveFirst();   // go to first record

            // don't allow add / delete when Employees are filtered 
            bindingNavigatorAddNewItem.Enabled = false;
            
        }
    }
}
